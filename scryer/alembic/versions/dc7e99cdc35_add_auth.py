"""add-auth

Revision ID: dc7e99cdc35
Revises: 220fe77158d9
Create Date: 2014-08-22 16:30:48.221850

"""

# revision identifiers, used by Alembic.
revision = 'dc7e99cdc35'
down_revision = '220fe77158d9'

from datetime import datetime
from passlib.hash import pbkdf2_sha256

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        U = sa.Table("user", sa.MetaData(),
                     sa.Column('ins_date', sa.DateTime(), nullable=False),
                     sa.Column('mod_date', sa.DateTime(), nullable=False),
                     sa.Column('id', sa.Integer(), nullable=False),
                     sa.Column('username', sa.Unicode(length=80),
                               nullable=False),
                     sa.Column('password', sa.Unicode(length=80),
                               nullable=False))

        op.create_table('user',
                        sa.Column('ins_date', sa.DateTime(), nullable=False),
                        sa.Column('mod_date', sa.DateTime(), nullable=False),
                        sa.Column('id', sa.Integer(), nullable=False),
                        sa.Column('username', sa.Unicode(length=80),
                                  nullable=False),
                        sa.Column('password', sa.Unicode(length=80),
                                  nullable=False),
                        sa.PrimaryKeyConstraint('id'),
                        sa.UniqueConstraint('username'))

        op.bulk_insert(U, [
            {"username": "admin",
             "password": pbkdf2_sha256.encrypt("test", rounds=40, salt_size=10),
             "ins_date": datetime.now(),
             "mod_date": datetime.now()}])


def downgrade(pyramid_env):
    with context.begin_transaction():
        op.drop_table('user')
