"""add_city_table

Revision ID: 543644a7f013
Revises: 37adbcaf1431
Create Date: 2015-03-17 13:24:08.126576

"""

# revision identifiers, used by Alembic.
revision = '543644a7f013'
down_revision = '37adbcaf1431'

from derby.sqla import PGTrigramIndex

from alembic import context, op

import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        C = sa.Table("city", sa.MetaData(),
                     sa.Column("name", sa.Unicode()),
                     sa.Column("province", sa.Unicode()))
        op.create_table("city",
                        sa.Column("name", sa.Unicode, primary_key=True),
                        sa.Column("province", sa.Unicode, primary_key=True),
                        sa.Column("lon", sa.Float),
                        sa.Column("lat", sa.Float),
                        sa.Column('ins_date', sa.DateTime(), nullable=False),
                        sa.Column('mod_date', sa.DateTime(), nullable=False))
        (PGTrigramIndex("city_search_purpose_ix", C.c.name)
         .create(op.get_bind()))
        (PGTrigramIndex("city_province_search_purpose_ix", C.c.province)
         .create(op.get_bind()))

        op.add_column("grant",
                      sa.Column("lon", sa.Float))
        op.add_column("grant",
                      sa.Column("lat", sa.Float))


def downgrade(pyramid_env):
    with context.begin_transaction():
        op.drop_table("city")
        op.drop_column("grant", "lon")
        op.drop_column("grant", "lat")
