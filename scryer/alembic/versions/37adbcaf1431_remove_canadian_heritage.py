"""remove_canadian_heritage

Revision ID: 37adbcaf1431
Revises: 45e744aa12b
Create Date: 2014-11-18 15:27:23.689471

"""

# revision identifiers, used by Alembic.
revision = '37adbcaf1431'
down_revision = '45e744aa12b'

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        G = sa.Table("grant", sa.MetaData(),
                     sa.Column("source", sa.Unicode, index=True))
        op.execute(G.delete().where(G.c.source == "heritage"))


def downgrade(pyramid_env):
    with context.begin_transaction():
        pass
