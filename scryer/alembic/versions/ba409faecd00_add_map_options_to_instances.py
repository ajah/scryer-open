"""add map options to instances

Revision ID: ba409faecd00
Revises: 35965dfcc11f
Create Date: 2016-04-14 14:28:22.388061

"""

# revision identifiers, used by Alembic.
revision = 'ba409faecd00'
down_revision = '35965dfcc11f'

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
      op.add_column('instance', 
        sa.Column('map_options_json', sa.String())
      )


def downgrade(pyramid_env):
    with context.begin_transaction():
        pass
