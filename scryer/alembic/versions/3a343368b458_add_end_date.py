"""add-end-date

Revision ID: 3a343368b458
Revises: dc7e99cdc35
Create Date: 2014-08-25 11:37:37.357823

"""

# revision identifiers, used by Alembic.
revision = '3a343368b458'
down_revision = 'dc7e99cdc35'

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        op.add_column("grant", sa.Column("end_date", sa.DateTime, nullable=True))


def downgrade(pyramid_env):
    with context.begin_transaction():
        op.drop_column("grant", "end_date")
