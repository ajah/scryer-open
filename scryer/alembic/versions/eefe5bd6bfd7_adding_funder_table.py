"""adding funder table

Revision ID: eefe5bd6bfd7
Revises: d09a718fd990
Create Date: 2016-08-04 09:26:04.008870

"""

# revision identifiers, used by Alembic.
revision = 'eefe5bd6bfd7'
down_revision = 'd09a718fd990'

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        op.create_table('funder',
                        sa.Column("id", sa.Unicode(24), nullable=False),
                        sa.Column('code', sa.Unicode(), nullable=False),
                        sa.Column('label', sa.Unicode(), nullable=False),
                        sa.Column('ins_date', sa.DateTime(), nullable=False),
                        sa.Column('mod_date', sa.DateTime(), nullable=False),
                        sa.PrimaryKeyConstraint('id'))
        op.add_column("grant",
                      sa.Column("funder_id", sa.Unicode(24), sa.ForeignKey('funder.id')))


def downgrade(pyramid_env):
    with context.begin_transaction():
        op.drop_column("grant", "funder_id")
        op.drop_table("funder")
