"""simplify-default-search-index

Revision ID: 45e744aa12b
Revises: 233033f011b4
Create Date: 2014-08-26 11:17:48.009595

"""

# revision identifiers, used by Alembic.
revision = '45e744aa12b'
down_revision = '233033f011b4'

from derby.sqla import PGTrigramIndex

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        G = sa.Table("grant", sa.MetaData(),
                     sa.Column("name", sa.Unicode(), nullable=True),
                     sa.Column("purpose", sa.Unicode(), nullable=True),
                     sa.Column("comments", sa.Unicode(), nullable=True))

        op.drop_index("grant_purpose_comments_search_ix",
                      table_name="grant")

        (PGTrigramIndex("grant_search_purpose_ix", G.c.purpose)
         .create(op.get_bind()))


def downgrade(pyramid_env):
    with context.begin_transaction():
        G = sa.Table("grant", sa.MetaData(),
                     sa.Column("name", sa.Unicode(), nullable=True),
                     sa.Column("purpose", sa.Unicode(), nullable=True),
                     sa.Column("comments", sa.Unicode(), nullable=True))

        op.drop_index("grant_search_purpose_ix", table_name="grant")

        (PGTrigramIndex("grant_purpose_comments_search_ix",
                        G.c.purpose, G.c.comments)
         .create(op.get_bind()))
