"""switched Alberta grants to ab slug

Revision ID: d09a718fd990
Revises: ba409faecd00
Create Date: 2016-07-29 19:50:39.129094

"""

# revision identifiers, used by Alembic.
revision = 'd09a718fd990'
down_revision = 'ba409faecd00'

from alembic import context, op
import sqlalchemy as sa

from scryer import models as m


def upgrade(pyramid_env):
    with context.begin_transaction():
        S = sa.Table("grant", sa.MetaData(),
                 sa.Column("province", sa.Unicode))
        
         
        op.execute(S.update().values(province="ab")
                .where(S.c.province == "Alberta"))

def downgrade(pyramid_env):
    with context.begin_transaction():
        pass
