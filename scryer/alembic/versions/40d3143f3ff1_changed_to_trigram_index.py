"""changed-to-trigram-index

Revision ID: 40d3143f3ff1
Revises: 4a4c2930a1c9
Create Date: 2014-08-19 18:06:32.058058

"""

# revision identifiers, used by Alembic.
revision = '40d3143f3ff1'
down_revision = '4a4c2930a1c9'

from derby.sqla import PGSearchIndex, PGTrigramIndex

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        op.drop_index("grant_search_ix", table_name="grant")
        op.drop_index("grant_name_purpose_comments_search_ix",
                      table_name="grant")

        G = sa.Table("grant", sa.MetaData(),
                     sa.Column("name", sa.Unicode(), nullable=True),
                     sa.Column("purpose", sa.Unicode(), nullable=True),
                     sa.Column("comments", sa.Unicode(), nullable=True))

        PGTrigramIndex('grant_search_ix', G.c.name).create(op.get_bind())

        (PGTrigramIndex("grant_name_purpose_comments_search_ix",
                        G.c.name, G.c.purpose, G.c.comments)
         .create(op.get_bind()))


def downgrade(pyramid_env):
    with context.begin_transaction():
        op.drop_index("grant_search_ix", table_name="grant")
        op.drop_index("grant_name_purpose_comments_search_ix",
                      table_name="grant")

        G = sa.Table("grant", sa.MetaData(),
                     sa.Column("name", sa.Unicode(), nullable=True),
                     sa.Column("purpose", sa.Unicode(), nullable=True),
                     sa.Column("comments", sa.Unicode(), nullable=True))

        PGSearchIndex('grant_search_ix', G.c.name).create(op.get_bind())

        (PGSearchIndex("grant_name_purpose_comments_search_ix",
                       G.c.name, G.c.purpose, G.c.comments)
         .create(op.get_bind()))
