"""add-grant-model

Revision ID: 2f8819950b75
Revises: 1743a2439ed6
Create Date: 2014-08-18 15:26:24.083573

"""

# revision identifiers, used by Alembic.
revision = '2f8819950b75'
down_revision = '1743a2439ed6'

from derby.sqla import PGSearchIndex, create_f_unaccent
from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        op.execute(create_f_unaccent)

        G = sa.Table("grant", sa.MetaData(),
                     sa.Column('name', sa.Unicode(), nullable=True))

        P = sa.Table("project", sa.MetaData(),
                        sa.Column('purpose', sa.Unicode(), nullable=True),
                        sa.Column('comments', sa.Unicode(), nullable=True))

        op.create_table('grant',
                        sa.Column('ins_date', sa.DateTime(), nullable=False),
                        sa.Column('mod_date', sa.DateTime(), nullable=False),
                        sa.Column('id', sa.Unicode(length=24), nullable=False),
                        sa.Column('source', sa.Unicode(), nullable=True),
                        sa.Column('code', sa.Unicode(length=40), nullable=True),
                        sa.Column('name', sa.Unicode(), nullable=True),
                        sa.Column('city', sa.Unicode(), nullable=True),
                        sa.Column('country', sa.Unicode(), nullable=True),
                        sa.Column('province', sa.Unicode(), nullable=True),
                        sa.Column('date', sa.DateTime(), nullable=False),
                        sa.Column('amount', sa.BigInteger(), nullable=True),
                        sa.PrimaryKeyConstraint('id'))

        PGSearchIndex('grant_search_ix', G.c.name).create(op.get_bind())

        op.create_index(u'ix_grant_code', 'grant', ['code'], unique=False)
        op.create_index(u'ix_grant_source', 'grant', ['source'], unique=False)

        op.create_table('project',
                        sa.Column('id', sa.Integer(), nullable=False),
                        sa.Column('ins_date', sa.DateTime(), nullable=False),
                        sa.Column('mod_date', sa.DateTime(), nullable=False),
                        sa.Column('grant_id', sa.Unicode(length=24), nullable=False),
                        sa.Column('lang', sa.Unicode(), nullable=False),
                        sa.Column('type', sa.Unicode(), nullable=True),
                        sa.Column('purpose', sa.Unicode(), nullable=True),
                        sa.Column('links', sa.Unicode(), nullable=True),
                        sa.Column('comments', sa.Unicode(), nullable=True),
                        sa.ForeignKeyConstraint(['grant_id'], ['grant.id']),
                        sa.PrimaryKeyConstraint('id'))

        op.create_index(u'ix_project_type', 'project', ['type'], unique=False)
        op.create_index(u'ix_project_grant_id', 'project', ['grant_id'],
                        unique=False)

        (PGSearchIndex('project_comments_search_ix', P.c.comments)
         .create(op.get_bind()))

        (PGSearchIndex('project_purpose_search_ix', P.c.purpose)
         .create(op.get_bind()))


def downgrade(pyramid_env):
    with context.begin_transaction():
        op.drop_index('project_purpose_search_ix', table_name='project')
        op.drop_index('project_comments_search_ix', table_name='project')
        op.drop_index(u'ix_project_type', table_name='project')
        op.drop_index(u'ix_project_grant_id', table_name='project')
        op.drop_table('project')
        op.drop_index(u'ix_grant_source', table_name='grant')
        op.drop_index(u'ix_grant_code', table_name='grant')
        op.drop_index('grant_search_ix', table_name='grant')
        op.drop_table('grant')
