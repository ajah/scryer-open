"""add instance description field

Revision ID: 35965dfcc11f
Revises: db748b40d05d
Create Date: 2016-04-14 13:34:06.073351

"""

# revision identifiers, used by Alembic.
revision = '35965dfcc11f'
down_revision = 'db748b40d05d'

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
      op.add_column('instance',
        sa.Column('description', sa.String())
      )


def downgrade(pyramid_env):
    with context.begin_transaction():
        pass
