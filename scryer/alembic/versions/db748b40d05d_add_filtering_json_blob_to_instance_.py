"""add filtering json blob to instance table

Revision ID: db748b40d05d
Revises: 5c85642b96a2
Create Date: 2016-03-30 16:29:27.543814

"""

# revision identifiers, used by Alembic.
revision = 'db748b40d05d'
down_revision = '5c85642b96a2'

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        op.add_column('instance', 
          sa.Column('filter_json', sa.Unicode())
        )


def downgrade(pyramid_env):
    with context.begin_transaction():
        pass
