"""add-city-search-index

Revision ID: 25d05a7ce557
Revises: 3a343368b458
Create Date: 2014-08-25 16:27:33.635743

"""

# revision identifiers, used by Alembic.
revision = '25d05a7ce557'
down_revision = '3a343368b458'

from derby.sqla import PGTrigramIndex

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        G = sa.Table("grant", sa.MetaData(),
                     sa.Column("city", sa.Unicode(), nullable=True))

        PGTrigramIndex('grant_search_city_ix', G.c.city).create(op.get_bind())


def downgrade(pyramid_env):
    with context.begin_transaction():
        op.drop_index("grant_search_city_ix", table_name="grant")
