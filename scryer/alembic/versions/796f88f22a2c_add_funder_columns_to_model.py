"""add_funder_columns_to_model

Revision ID: 796f88f22a2c
Revises: eefe5bd6bfd7
Create Date: 2016-08-11 11:51:36.321218

"""

# revision identifiers, used by Alembic.
revision = '796f88f22a2c'
down_revision = 'eefe5bd6bfd7'

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        op.add_column('funder', sa.Column('description', sa.Unicode()))
        op.add_column('funder', sa.Column('url', sa.Unicode()))
        op.add_column('funder', sa.Column('address_line_1', sa.Unicode()))
        op.add_column('funder', sa.Column('address_line_2', sa.Unicode()))
        op.add_column('funder', sa.Column('province', sa.Unicode()))
        op.add_column('funder', sa.Column('postal_code', sa.Unicode()))
        op.add_column('funder', sa.Column('country', sa.Unicode()))


def downgrade(pyramid_env):
    with context.begin_transaction():
        pass
