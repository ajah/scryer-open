"""remove-name-from-search-index

Revision ID: 220fe77158d9
Revises: 40d3143f3ff1
Create Date: 2014-08-21 15:55:07.539406

"""

# revision identifiers, used by Alembic.
revision = '220fe77158d9'
down_revision = '40d3143f3ff1'

from derby.sqla import PGTrigramIndex

from alembic import context, op

import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        G = sa.Table("grant", sa.MetaData(),
                     sa.Column("name", sa.Unicode(), nullable=True),
                     sa.Column("purpose", sa.Unicode(), nullable=True),
                     sa.Column("comments", sa.Unicode(), nullable=True))

        op.drop_index("grant_name_purpose_comments_search_ix",
                      table_name="grant")
        (PGTrigramIndex("grant_purpose_comments_search_ix",
                        G.c.purpose, G.c.comments)
         .create(op.get_bind()))


def downgrade(pyramid_env):
    with context.begin_transaction():
        G = sa.Table("grant", sa.MetaData(),
                     sa.Column("name", sa.Unicode(), nullable=True),
                     sa.Column("purpose", sa.Unicode(), nullable=True),
                     sa.Column("comments", sa.Unicode(), nullable=True))

        op.drop_index("grant_purpose_comments_search_ix",
                      table_name="grant")

        (PGTrigramIndex("grant_name_purpose_comments_search_ix",
                        G.c.name, G.c.purpose, G.c.comments)
         .create(op.get_bind()))
