"""create instance table

Revision ID: 97633cebc356
Revises: 543644a7f013
Create Date: 2016-03-30 14:37:35.809794

"""

# revision identifiers, used by Alembic.
revision = '97633cebc356'
down_revision = '543644a7f013'

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
  op.create_table(
    'instance',
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('name', sa.Unicode, nullable=False),
    sa.Column('slug', sa.String, nullable=False)
  )


def downgrade(pyramid_env):
    with context.begin_transaction():
        pass
