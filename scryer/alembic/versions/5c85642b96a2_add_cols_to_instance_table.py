"""add cols to instance table

Revision ID: 5c85642b96a2
Revises: 97633cebc356
Create Date: 2016-03-30 14:51:36.323981

"""

# revision identifiers, used by Alembic.
revision = '5c85642b96a2'
down_revision = '97633cebc356'

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
      op.add_column('instance',
        sa.Column('mod_date', sa.DateTime(), nullable=False)
      )
      op.add_column('instance',
        sa.Column('ins_date', sa.DateTime(), nullable=False)
      )

def downgrade(pyramid_env):
    with context.begin_transaction():
        pass
