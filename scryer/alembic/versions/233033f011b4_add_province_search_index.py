"""add-province-search-index

Revision ID: 233033f011b4
Revises: 25d05a7ce557
Create Date: 2014-08-26 10:08:08.199133

"""

# revision identifiers, used by Alembic.
revision = '233033f011b4'
down_revision = '25d05a7ce557'

from derby.sqla import PGTrigramIndex

from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        G = sa.Table("grant", sa.MetaData(),
                     sa.Column("province", sa.Unicode(), nullable=True))

        PGTrigramIndex('grant_search_province_ix', G.c.province).create(
            op.get_bind())


def downgrade(pyramid_env):
    with context.begin_transaction():
        op.drop_index("grant_search_province_ix", table_name="grant")
