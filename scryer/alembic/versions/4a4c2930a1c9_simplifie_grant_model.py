"""simplifie-grant-model

Revision ID: 4a4c2930a1c9
Revises: 2f8819950b75
Create Date: 2014-08-19 15:53:30.974695

"""

# revision identifiers, used by Alembic.
revision = "4a4c2930a1c9"
down_revision = "2f8819950b75"

from derby.sqla import PGSearchIndex
from alembic import context, op
import sqlalchemy as sa


def upgrade(pyramid_env):
    with context.begin_transaction():
        G = sa.Table("grant", sa.MetaData(),
                     sa.Column("name", sa.Unicode(), nullable=True),
                     sa.Column("purpose", sa.Unicode(), nullable=True),
                     sa.Column("comments", sa.Unicode(), nullable=True))

        op.drop_index("project_purpose_search_ix", table_name="project")
        op.drop_index("project_comments_search_ix", table_name="project")
        op.drop_index(u"ix_project_type", table_name="project")
        op.drop_index(u"ix_project_grant_id", table_name="project")
        op.drop_table("project")

        op.add_column("grant", sa.Column("type", sa.Unicode(),
                                         nullable=True))

        op.add_column("grant", sa.Column("purpose", sa.Unicode(),
                                         nullable=True))

        op.add_column("grant", sa.Column("links", sa.Unicode(),
                                         nullable=True))

        op.add_column("grant", sa.Column("comments", sa.Unicode(),
                                         nullable=True))

        op.create_index(u"ix_grant_type", "grant", ["type"], unique=False)

        (PGSearchIndex("grant_name_purpose_comments_search_ix",
                       G.c.name, G.c.purpose, G.c.comments)
         .create(op.get_bind()))


def downgrade(pyramid_env):
    with context.begin_transaction():
        P = sa.Table("project", sa.MetaData(),
                     sa.Column("purpose", sa.Unicode(), nullable=True),
                     sa.Column("comments", sa.Unicode(), nullable=True))

        op.create_table("project",
                        sa.Column("id", sa.Integer(), nullable=False),
                        sa.Column("ins_date", sa.DateTime(), nullable=False),
                        sa.Column("mod_date", sa.DateTime(), nullable=False),
                        sa.Column("grant_id", sa.Unicode(length=24), nullable=False),
                        sa.Column("lang", sa.Unicode(), nullable=False),
                        sa.Column("type", sa.Unicode(), nullable=True),
                        sa.Column("purpose", sa.Unicode(), nullable=True),
                        sa.Column("links", sa.Unicode(), nullable=True),
                        sa.Column("comments", sa.Unicode(), nullable=True),
                        sa.ForeignKeyConstraint(["grant_id"], ["grant.id"]),
                        sa.PrimaryKeyConstraint("id"))

        op.create_index(u"ix_project_type", "project", ["type"], unique=False)
        op.create_index(u"ix_project_grant_id", "project", ["grant_id"],
                        unique=False)

        (PGSearchIndex("project_comments_search_ix", P.c.comments)
         .create(op.get_bind()))

        (PGSearchIndex("project_purpose_search_ix", P.c.purpose)
         .create(op.get_bind()))

        op.drop_column("grant", "type")
        op.drop_column("grant", "purpose")
        op.drop_column("grant", "links")
        op.drop_column("grant", "comments")
