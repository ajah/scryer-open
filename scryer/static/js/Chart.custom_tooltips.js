window.count = 0;
Chart.defaults.global.pointHitDetectionRadius = 1;
var customTooltips = function(tooltip) {
      // Tooltip Element
      var tooltipEl = $('#chartjs-tooltip');
      if (!tooltipEl[0]) {
        $('body').append('<div id="chartjs-tooltip"></div>');
        tooltipEl = $('#chartjs-tooltip');
      }
      // Hide if no tooltip
      if (!tooltip.opacity) {
        tooltipEl.css({
          opacity: 0
        });
        $('.chartjs-wrap canvas')
          .each(function(index, el) {
            $(el).css('cursor', 'default');
          });
        return;
      }
      $(this._chart.canvas).css('cursor', 'pointer');
      // Set caret Position
      tooltipEl.removeClass('above below no-transform');
      if (tooltip.yAlign) {
        tooltipEl.addClass(tooltip.yAlign);
      } else {
        tooltipEl.addClass('no-transform');
      }

      // Set Text
      if (tooltip.body) {
          values  = tooltip.body.toString().split(':');
          formated_values  = values[0] + ': ' + parseInt(parseFloat(values[1]) * 100) + '%';
          tooltipEl.html(formated_values);
      }
      
      var position = $(this._chart.canvas)[0].getBoundingClientRect();
      // Display, position, and set styles for font
      tooltipEl.css({
        position: 'absolute',
        display: "inline",
        backgroundColor: "#000",
        color: "#fff",
        zIndex: 9999,
        opacity: 1,
        width: "auto",
        left:  tooltip.x + 'px',
        top:  tooltip.y + 'px',
        fontFamily: tooltip._fontFamily,
        fontSize: tooltip.fontSize,
        fontStyle: tooltip._fontStyle,
        padding: tooltip.yPadding + 'px ' + tooltip.xPadding + 'px',
      });
    };
