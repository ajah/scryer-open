var gMap;
var gCenterMarker;
var Icon;
var gSlugChanged = false;

function init(opt) {
  initIcon();
  initMap(opt.mapOptions);
  initAutoslug(opt.instanceExists);
}

function initIcon() {
  L.Icon.Default.imagePath = "/static";
  Icon = L.Icon.Default.extend({
      options: {
          iconUrl: "/static/marker-icon-green.png",
          iconSize: [32, 43]
      }
  });
}

function initMap(mapOptions) {
  var options = $.extend({}, {
      attributionControl: false,
      center: [50.2671198,-98.9729664],
      zoom: 3
    }, {
      center: mapOptions.center,
      zoom: mapOptions.zoom
    });

  gMap = L.map('map', options);
  L.tileLayer.provider("MapBox.ajah.g741mhgh").addTo(gMap);

  if(mapOptions.center) {
    gCenterMarker = _createMarker(mapOptions.center);
  }

  gMap.on('click', function(e) {
    // set zoom too on click (as zoom may never be changed by the user)
    _setMapOption({
      center: e.latlng,
      zoom: gMap.getZoom()
    });

    if(!gCenterMarker) {
      gCenterMarker = _createMarker(e.latlng);
    } else {
      gCenterMarker.setLatLng(e.latlng);
    }
  });

  gMap.on('zoomend', function() {
    _setMapOption({
      zoom: gMap.getZoom()
    })
  });
}

function _createMarker(center) {
  return L.marker(center, {
    icon: new Icon()
  }).addTo(gMap);
}

function _setMapOption(opt) {
  var mapOptions = $('form input[name=mapOptions]').val()
  mapOptions = JSON.parse(mapOptions);
  $.extend(mapOptions, opt);
  $('form input[name=mapOptions]').val(JSON.stringify(mapOptions)); 
}


function slugify(text)
{
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}

function initAutoslug(instanceExists) {
  var originalSlugValue;

  if(instanceExists) {
    originalSlugValue = $('input[name=slug]').val()
  }

  // if slug is manually-edited, stop auto-slugging
  $('input[name=slug]').on('input', function() {
    gSlugChanged = true;

    // show/hide warning if the slug value has changed
    if(instanceExists) {
      $('#slugChangeWarning').toggle($(this).val() !== originalSlugValue)
    }
  });

  $('input[name=name]').on('input', function() {
    // if this is an existing instance or if the user has
    // manually changed the slug
    if(instanceExists || gSlugChanged) {
      return;
    }

    $('input[name=slug]').val(slugify($(this).val()));
  });
}