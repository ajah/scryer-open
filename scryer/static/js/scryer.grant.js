var App = flight.component(function() {
    var $this = this;

    var getRandomInt = function(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    this.after("initialize", function() {
        $this.component = this;
        $this.client = new $.RestClient("/");
        $this.client.add("data");
        $this.suggestion_url = $this.component.attr.suggestion_url || "/suggestion";
        $this.table_url = $this.component.attr.grant_table_url || "/grants-table";
        $this.modal_url = $this.component.attr.grant_modal_url || "/grants-modal";
        $this.modal_table_url = $this.component.attr.grant_modal_table_url || "/grants-modal-table";
        $this.summary_url = $this.component.attr.grant_summary_url || "/grants-summary";
        $this.city_suggestion_url = $this.component.attr.city_suggestion_url || "/city-suggestion";
        $this.donut_data_url = $this.component.attr.donut_data_url || "/donut-data";
        $this.search_form = $("#search-form");
        $this.$source = $("#source");
        $this.$search = $("#search");
        $this.$name = $("#name");
        $this.$city = $("#city");
        $this.$province = $("#province");
        $this.$sort = $(".grant-sort");
        $this.$date_to = $("#date_to");
        $this.$date_from = $("#date_from");
        $this.$map_tab = $("#map-tab");
        $this.$donut_chart = $("#canvas");
        $this.$donut_chart_legend = $("#js-legend");
        $this.$doughnut_chart_instance = null
        $this.$grant_map = $("#grant-map");
        $this.$grant_summary = $("#grant-summary");
        $this.$grant_table = $("#grant-table");
        $this.$grant_map = $("#grant-map");
        $this.$grant_export = $("#grant-export");

        $this.modal_zoom_level = 7;

        $this.MapOptions = $.extend({
            zoomControl: false,
            maxZoom: 8,
            attributionControl: false,
            center: [59.7233268, -96.5318048],
            zoom: 3
        }, $this.component.attr.map_options);
        $this.MarkerClusterOptions = {
            spiderfyOnMaxZoom: false,
            showCoverageOnHover: false
        };
        $this.$loader = $("#loader");
        $this.$results = $("#results");

        $this.$map_tab.on('shown.bs.tab', function (e) {
            if(!$this.$map_tab.hasClass("disabled")) {
                $this.map.removeLayer($this.markers);
                $this.map.addLayer($this.markers);
                $this.map.invalidateSize();
            }
        });

        $this.initSearch();
        $this.initMap();
    });

    this.initMap = function(options) {
        $this.mapBoxId = "MapBox.ajah.g741mhgh";
        $this.map = L.map("grant-map", $this.MapOptions);

        L.tileLayer.provider($this.mapBoxId, {
            attribution: false
        }).addTo($this.map);

        new L.Control.Zoom({
            position: "topright"
        }).addTo($this.map);
        ($(".leaflet-container")
         .css("height", ($("body").height() - $("#header").height()) * 0.40));
        $( window ).resize(function() {
            $(".leaflet-container").css("height", ($("body").height() - $("#header").height()) * 0.40)
        });
        L.Icon.Default.imagePath = "/static";
        $this.Icon = L.Icon.Default.extend({
            options: {
                iconUrl: "/static/marker-icon-green.png",
                iconSize: [32,43]
            }
        });
        $this.markers = new L.MarkerClusterGroup($this.MarkerClusterOptions);
    }


    this.getDonutChart = function(){
        if ($this.$doughnut_chart_instance) {
            $this.$doughnut_chart_instance.destroy();
        }

        var donut = $.get($this.donut_data_url + '?' + $this.search_form.serialize() , function(data) {
            if ( "donut_data" in data) {
                 var donut_data = {
                    labels: $.map(data['donut_data'], function(item, key) {
                        return key;
                    }),
                    datasets: [
                        {
                        data : $.map(data['donut_data'], function(item, key) {
                        return item.toFixed(2);
                        }),
                        backgroundColor: [
                            "#808080", 
                            "#008B8B", 
                            "#663399", 
                            "#36A2EB", 
                            "#F08080", 
                            "#FF9C00", 
                            "#36eb5c", 
                            "#028102", 
                            "#021481", 
                            "#810202", 
                            "#ebe536", 
                            "#eb3636", 
                            "#b280f0" 
                        ],
                    }]
                };
                $this.$doughnut_chart_instance = new Chart($this.$donut_chart, {
                    type: 'doughnut',
                    data: donut_data,
                    options: {
                        responsive: true,
                        legend:{
                            display: false
                        },
                        tooltips: {
                            enabled: false,
                            custom: customTooltips
                         }
                    }
                });
            }
        }).always(function() {
            $this.$donut_chart_legend.html($this.$doughnut_chart_instance.generateLegend());
        });
    }

    this.refreshMap = function() {
        $this.client.data.create($this.component.attr).done(function (data, textStatus, xhrObject) {
            $this.enable_map_link();
            $this.map.removeLayer($this.markers);
            $this.markers = new L.MarkerClusterGroup($this.MarkerClusterOptions);

            var ids = [];

            data.pager.forEach(function(item) {
                // var lat = getRandomInt(42, 83);
                // var lon = getRandomInt(-53, -141);
                if(item.lat && item.lon) {
                    $this.markers.addLayer(new L.Marker([item.lat, item.lon], {
                        icon: new $this.Icon(),
                        grant_id: item.grant_id
                    }));
                    ids.push(item.grant_id);
                }
            });
            $this.map.addLayer($this.markers);
            $this.map.invalidateSize();

            $this.markers.on("click", function (e) {
                $this.showModal([e.layer.options.grant_id]);
            });

            $this.markers.on("clusterclick", function (e) {
                if ($this.map.getZoom() > $this.modal_zoom_level) {
                    var ids = []
                    e.layer.getAllChildMarkers().forEach(function(item) {
                        if(item.options.grant_id) {
                            ids.push(item.options.grant_id);
                        }
                    });
                    $this.showModal(ids);
                }
            });
        });
    }

    this.initSearch = function() {
        $this.$search.on("keypress", function(e) {
            if(e.which == 13 || !e.which) {
                $this.updateVals({
                    search: $this.$search.val(),
                    name: $this.$name.val(),
                    city: $this.$city.val(),
                });
            }
        });
        $this.$search.on("change", function(e) {
            $this.updateVals({
                search: $this.$search.val(),
                name: $this.$name.val(),
                city: $this.$city.val(),
            });
        });
        $this.$source.on("change", function(e) {
            $this.updateVals({
                source: $this.$source.val()
            });
        });
        $this.$province.on("change", function(e) {
            $this.updateVals({
                province: $this.$province.val()
            });
        });
        $this.$name.on("change", function(e) {
            $this.updateVals({
                name: $this.$name.val()
            });
        });
        $this.$name.on("keypress", function(e) {
            if(e.which == 13 || !e.which) {
                $this.updateVals({
                    search: $this.$search.val(),
                    name: $this.$name.val(),
                    city: $this.$city.val()
                });
            }
        });
        $this.$city.on("keypress", function(e) {
            if(e.which == 13 || !e.which) {
                $this.updateVals({
                    search: $this.$search.val(),
                    name: $this.$name.val(),
                    city: $this.$city.val(),
                });
            }
        });
        $this.$sort.on("click", function(e) {
            $this.updateVals({
                sort: $(e.target).data("sort")
            });
            e.preventDefault();

            return false;
        });
        $this.$date_from.on("change", function(e) {
            $this.updateVals({
                date_from: $this.$date_from.val()
            });
        });
        $this.$date_to.on("change", function(e) {
            $this.updateVals({
                date_to: $this.$date_to.val()
            });
        });
        $this.$name.autocomplete({
            serviceUrl: $this.suggestion_url,
            minChars: 4,
            onSelect: function (suggestion) {
                $this.updateVals({
                    name: suggestion.value
                });
            }
        });
        $this.$city.autocomplete({
            serviceUrl: $this.city_suggestion_url,
            minChars: 4,
            onSelect: function (suggestion) {
                $this.updateVals({
                    city: suggestion.value
                });
            }
        });
        $this.$search.trigger("keypress");
    }

    this.refreshResults = function() {
        // if in instance section, return pagination accordingly
        if (window.location.pathname.indexOf("instance") > 0) {
            url_split = window.location.pathname.split("/");
            instance_slug = url_split[2];
        }
        else {
            instance_slug = "";
        }

        var params = {
            limit: $this.component.attr.limit,
            source: $this.component.attr.source,
            page: $this.component.attr.page,
            search: $this.component.attr.search,
            name: $this.component.attr.name,
            sort: $this.component.attr.sort,
            city: $this.component.attr.city,
            province: $this.component.attr.province.toString(),
            date_from: $this.component.attr.date_from,
            date_to: $this.component.attr.date_to,
            instance_slug: instance_slug
        };

        var query = decodeURIComponent($.param(params));

        $this.$source.val($this.component.attr.source);
        $this.$search.val($this.component.attr.search);
        $this.$name.val($this.component.attr.name);
        $this.$city.val($this.component.attr.city);
        //$this.$province.val($this.component.attr.province);

        $this.$date_from.val($this.component.attr.date_from)
        $this.$date_to.val($this.component.attr.date_to)
        $this.update_history(query, params);
        $this.display_loader();

        $this.$grant_export.attr("href", $this.$grant_export.data("url") + "?" + query)

        $.get($this.summary_url+"?"+query, function(d) {
            $this.$grant_summary.html(d);
        });

        $.get($this.table_url+"?"+query, function(d) {
            $this.$grant_table.html(d);
            $this.hide_loader();

            $(".grant-source").on("click", function(e) {
                $this.updateVals({
                    source: $(this).data("value"),
                    search: "",
                    name: ""
                });
                e.preventDefault();
                return false;
            });
            $(".grant-province").on("click", function(e) {
                $this.updateVals({
                    province: $(this).html(),
                    search: "",
                    name: ""
                });

                e.preventDefault();

                return false;
            });
            $(".grant-sort").on("click", function(e) {
                $this.updateVals({
                    sort: $(e.target).data("sort")
                });

                e.preventDefault();

                return false;
            });
            if($this.component.attr.sort){
                var field = $this.component.attr.sort.split('_')[0];
                $("#grant-table a[data-sort*='"+field+"']").parent().addClass("active");

                if ($this.component.attr.sort.indexOf('down')) {
                    $("#grant-table a[data-sort="+$this.component.attr.sort+"]").attr("data-sort", field + "_up");
                } else {
                    $("#grant-table a[data-sort="+$this.component.attr.sort+"]").attr("data-sort", field + "_down");
                }
            }else{
                $("#grant-table a[data-sort='name_down']").parent().addClass("active");
            }

            $("#grant-table ul.pagination").each(function(){
                var links = $(this);
                $(this).children().each(function(){
                    var tmp_container = $("<li></li>");
                    tmp_container.append($(this));
                    links.append(tmp_container);
                });
            });
             $("#grant-table ul.pagination span.pager_curpage").parent().addClass("active");

        });
        this.getDonutChart();
    }

    this.updateVals = function(vals) {
        if(vals.source != null) {
            $this.component.attr.source = vals.source;
        } else {
            $this.component.attr.source = vals.source || $this.component.attr.source;
        }
        if(vals.limit != null) {
            $this.component.attr.limit = vals.limit;
        } else {
            $this.component.attr.page = vals.page || $this.component.attr.page;
        }
        if(vals.search != null) {
            $this.component.attr.search = vals.search;
        } else {
            $this.component.attr.search = vals.search || $this.component.attr.search;
        }
        if(vals.name != null) {
            $this.component.attr.name = vals.name;
        } else {
            $this.component.attr.name = vals.name || $this.component.attr.name;
        }
        if(vals.city != null) {
            $this.component.attr.city = vals.city;
        } else {
            $this.component.attr.city = vals.city || $this.component.attr.city;
        }
        if(vals.province != null) {
            $this.component.attr.province = vals.province.toString();
        } else {
            $this.component.attr.province = vals.province || $this.component.attr.province;
        }
        if(vals.sort != null) {
            $this.component.attr.sort = vals.sort;
        } else {
            $this.component.attr.sort = vals.sort || $this.component.attr.sort;
        }
        if(vals.date_from != null) {
            $this.component.attr.date_from = vals.date_from;
        } else {
            $this.component.attr.date_from = vals.date_from || $this.component.attr.date_from;
        }
        if(vals.date_to != null) {
            $this.component.attr.date_to = vals.date_to;
        } else {
            $this.component.attr.date_to = vals.date_to || $this.component.attr.date_to;
        }

        $this.refreshResults();
        $this.refreshMap();
    }

    this.showModal = function(ids) {
        $.get($this.modal_url, function(data) {
            $(".modal").remove();
            $("body").append($(data, "#grant-modal"));
            $("#grant-modal").modal();
            $.ajax({
                type: "POST",
                url: $this.modal_table_url,
                data: {
                    ids: ids
                }
            }).success(function(table_data) {
                $("#modal-table-results").html(table_data);
            });
        });
    }

    this.update_history = function(query, params) {
        History.pushState(params, "Landscape", "?"+query)
    }

    this.display_loader = function() {
        $this.$loader.show();
        $this.$loader.css("display", "block");
        $this.$loader.parent().css("position", "relative");
    }

    this.hide_loader = function() {
        $this.$loader.hide();
        $this.$loader.css("display", "none");
        $this.$loader.parent().css("position", "absolute");
    }

    this.enable_map_link = function() {
        $this.$map_tab.attr("data-toggle", "tab");
        $this.$map_tab.removeAttr("class");
        $this.$map_tab.attr("href", "#map");
        $this.$map_tab.html("Map");
    }

    this.disable_map_link = function() {
        $this.$map_tab.removeAttr("data-toggle");
        $this.$map_tab.removeAttr("href");
        $this.$map_tab.attr("class", "disabled");
        $this.$map_tab.html($this.$loader[0]);
    }
});
