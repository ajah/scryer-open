# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import unicodecsv as csv
from StringIO import StringIO

from pyramid.renderers import JSON
from sqlalchemy.orm.query import Query


class CSVRenderer(object):
    def __init__(self, info):
        pass

    def __call__(self, value, system):
        fout = StringIO()
        writer = csv.writer(fout, delimiter=b",", quotechar=b'"',
                            encoding="utf8")
        writer.writerow(value["header"])
        writer.writerows(value["rows"])
        filename = value["filename"]

        resp = system["request"].response
        resp.content_type = b"text/csv"
        resp.content_disposition = b"attachment;filename=" + filename

        return fout.getvalue()


def sqlalchemy_query_json_adapter(obj, request):
    return obj.all()


def includeme(config):
    json_renderer = JSON(indent=4)
    json_renderer.add_adapter(Query, sqlalchemy_query_json_adapter)

    config.add_renderer("csv", "scryer.renderers.CSVRenderer")
    config.add_renderer("json", json_renderer)
