from __future__ import (absolute_import, division, print_function,
        unicode_literals)

import os
from contextlib import contextmanager
from os.path import dirname, realpath

from decorator import decorator


@contextmanager
def chdir(path):
    """Context manager that changes and restores the working directory."""
    old_dir = os.getcwd()
    os.chdir(path)
    yield
    os.chdir(old_dir)


@decorator
def chdir_to_src_root(func, *args, **kwargs):
    """Temporarily change the working directory to the project source's root
    for the duration of calling the provided function.
    """
    with chdir(dirname(dirname(dirname(realpath(__file__))))):
        return func(*args, **kwargs)
