# -*- coding: utf-8 -*-

from collections import OrderedDict

from unidecode import unidecode


PROVINCES = OrderedDict([
    ("ab", "Alberta"),
    ("bc", "British Columbia"),
    ("mb", "Manitoba"),
    ("nb", "New Brunswick"),
    ("nl", "Newfoundland and Labrado"),
    ("nu", "Nunavut"),
    ("ns", "Nova Scotia"),
    ("nt", "Northwest Territories"),
    ("on", "Ontario"),
    ("pe", "Prince-Edward Island"),
    ("qc", "Quebec"),
    ("SK", "Saskatchewan"),
    ("yt", "Yukon")])


MATCHES = {
    "ontario": "on",
    "quebec": "qc",
    "new-brunswick": "nb",
    "britishcolmbia": "bc",
    "aberta": "ab",
    "alberta": "ab",
    "(quebec)": "qc",
}


def clean_province(value):
    """ clean a province and return the cleaned version """

    if value:
        value = unidecode(value.strip().lower())
        if value in MATCHES:
            return MATCHES[value]
    return value
