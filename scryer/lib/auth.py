"""Security-related stuff."""

from __future__ import (absolute_import, division, print_function,
        unicode_literals)

from pyramid.authentication import AuthTktAuthenticationPolicy as AuthPol
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.security import Allow

from scryer.models import User


def groupfinder(userid, request):
    """Return the names of the groups a user belongs to."""
    if User.by_username(userid) is not None:
        return ["admins"]
    return []


class RootFactory(object):
    """Pyramid root factory for the context of requests."""
    __acl__ = [(Allow, 'admins', ('private'))]

    def __init__(self, request):
        pass


def includeme(config):
    """Initialize security in pyramid."""
    settings = config.registry.settings
    authpol = AuthPol(settings.get('pyramid.authpol.secret', 'secret'),
                      callback=groupfinder, hashalg='sha512')
    config.set_authentication_policy(authpol)
    config.set_authorization_policy(ACLAuthorizationPolicy())
    config.set_root_factory(RootFactory)
