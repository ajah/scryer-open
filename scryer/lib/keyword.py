# -*- coding: utf-8 -*-


def get_default_keyword(request):
    """ get the default keywords from the application settings """
    settings = request.registry.settings
    vals = settings.get("scryer.keywords", "")
    if vals:
        return vals.split(",")
    return ""
