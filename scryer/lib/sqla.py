"""Some utilities helping with using SQLAlchemy models."""

from __future__ import (absolute_import, division, print_function,
        unicode_literals)

import re
import sys
from datetime import datetime

from pytz import UTC
from sqlalchemy import Column, engine_from_config, event
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import mapper, scoped_session, sessionmaker
from sqlalchemy.orm.util import has_identity
from sqlalchemy.types import DateTime, TypeDecorator
from zope.sqlalchemy import ZopeTransactionExtension

from pyramid.paster import get_appsettings, setup_logging

_TABLENAME_RE = re.compile('([A-Z]+)')


def declarative_bases(metadata):
    """Return all declarative bases bound to a single metadata object."""
    registry = dict()
    return (declarative_base(cls=BaseOps, class_registry=registry,
                             metadata=metadata),
            declarative_base(cls=Timestamped, class_registry=registry,
                             metadata=metadata))


def make_session(with_zte=True):
    """Return a scoped session, optionally with ZopeTransactionExtension."""
    args = with_zte and dict(extension=ZopeTransactionExtension()) or dict()
    return scoped_session(sessionmaker(**args))


def create_engine(config_uri):
    """Return an SQLAlchemy engine configured as per the provided config."""
    setup_logging(config_uri)
    settings = get_appsettings(config_uri)
    engine = engine_from_config(settings, 'sqlalchemy.')
    return engine


class BaseOps(object):
    """Basic database operations are abstracted away in this class.

    The idea is to have the API as independent as practically possible from
    both data storage- and web- specific stuff.

    """
    @declared_attr
    def __tablename__(cls):
        """Return a table name made out of the model class name."""
        return _TABLENAME_RE.sub(r'_\1', cls.__name__).strip('_').lower()

    @declared_attr
    def db(cls):
        """Return the SQLAlchemy db session object."""
        from ..models import DBSession
        return DBSession

    def __iter__(self, **kwargs):
        """Return a generator that iterates through model columns."""
        return self.iteritems(**kwargs)

    def iteritems(self, include=None, exclude=None):
        """Return a generator that iterates through model columns.

        Fields iterated through can be specified with include/exclude.

        """
        if include is not None and exclude is not None:
            include = set(include) - set(exclude)
            exclude = None
        for c in self.__table__.columns:
            if ((not include or c.name in include)
            and (not exclude or c.name not in exclude)):
                yield(c.name, getattr(self, c.name))

    @classmethod
    def _col_names(cls):
        """Return a list of the columns, as a set."""
        return set(cls.__table__.c.keys())

    @classmethod
    def _pk_names(cls):
        """Return a list of the primary keys, as a set."""
        return set(cls.__table__.primary_key.columns.keys())

    @property
    def is_new(self):
        """Return True if the instance wasn't fetched from the database."""
        return not has_identity(self)

    @classmethod
    def create(cls, obj=None, flush=False, **values):
        if obj is None:
            obj = cls(**values)
        else:
            obj.update(**values)
        obj.save(flush)
        return obj

    @classmethod
    def get(cls, first=True, **criteria):
        """Return the record corresponding to the criteria.

        Throw an exception if record not found and `first` == False, else
        return None.

        """
        q = cls.db.query(cls).filter_by(**criteria)
        return q.first() if first else q.one()

    @classmethod
    def find(cls, **criteria):
        return cls.db.query(cls).filter_by(**criteria)

    def delete(self):
        self.db.delete(self)

    def update(self, **values):
        fields = self._col_names()
        for name, value in values.iteritems():
            if name in fields:
                setattr(self, name, value)

    def save(self, flush=False):
        if self.is_new:
            self.db.add(self)
        if flush:
            self.db.flush()

    @classmethod
    def inject_api(cls, name, as_object=False):
        """Inject common methods in an API module."""
        class Obj(object):
            pass

        container = Obj() if as_object else sys.modules[name]

        for attr in 'create', 'get', 'find', 'validator':
            setattr(container, attr, getattr(cls, attr))

        if as_object:
            return container


class Timestamped(BaseOps):
    """An automatically timestamped mixin."""
    ins_date = Column(DateTime, nullable=False, default=datetime.utcnow)
    mod_date = Column(DateTime, nullable=False, default=datetime.utcnow)
    _stamps = ['ins_date', 'mod_date']

    def iteritems(self, include=None, exclude=None):
        if exclude is None:
            exclude = self._stamps
        elif len(exclude) > 0:
            exclude = set(exclude) | set(self._stamps)
        return super(Timestamped, self).iteritems(exclude=exclude,
                                                  include=include)


class UTCDateTime(TypeDecorator):
    impl = DateTime

    def process_bind_param(self, value, engine):
        if value is not None:
            return value.astimezone(UTC).replace(tzinfo=None)

    def process_result_value(self, value, engine):
        if value is not None:
            return value.replace(tzinfo=UTC)


def insert_timestamp(mapper, connection, target):
    """Initialize timestamps on models that have these fields.

    Event handler for 'before_insert'.

    """
    timestamp = datetime.utcnow()
    if hasattr(target, 'ins_date'):
        target.ins_date = timestamp
    if hasattr(target, 'mod_date'):
        target.mod_date = timestamp


def update_timestamp(mapper, connection, target):
    """Update the modified date on models that have this field.

    Event handler for 'before_update'.

    """
    if hasattr(target, 'mod_date'):
        target.mod_date = datetime.utcnow()


event.listen(mapper, 'before_insert', insert_timestamp)
event.listen(mapper, 'before_update', update_timestamp)
