# -*- coding: utf-8 -*-

from pyramid.decorator import reify
from pyramid.request import Request
from pyramid.i18n import get_locale_name
from pyramid.security import authenticated_userid


class ScryerRequest(Request):
    """ Custom Request object """

    @reify
    def locale(self):
        """ Return the current locale. """
        return get_locale_name(self)

    @reify
    def user_id(self):
        return authenticated_userid(self)

    def route_active(self, name):
        """Return true of the current route is one we've expected."""
        return self.matched_route and self.matched_route.name == name
