# -*- coding: utf-8 -*-

from passlib.hash import pbkdf2_sha256

from sqlalchemy import ForeignKey, Integer, Table, Unicode, Column
from sqlalchemy.orm import validates

from . import TimestampedBase


class User(TimestampedBase):
    """Represents a user in the system."""
    id = Column(Integer, primary_key=True)

    username = Column(Unicode(80), nullable=False, unique=True)
    password = Column(Unicode(80))

    def __unicode__(self):
        return self.username

    @classmethod
    def by_id(cls, userid):
        return cls.db.query(User).filter(User.id == userid).first()

    @classmethod
    def by_username(cls, username):
        return cls.db.query(User).filter(User.username == username).first()

    @validates('password', include_removes=False)
    def set_password(self, key, password):
        return pbkdf2_sha256.encrypt(password, rounds=40, salt_size=10)

    def verify_password(self, password):
        return pbkdf2_sha256.verify(password, self.password)
