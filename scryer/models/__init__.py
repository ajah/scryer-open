# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime

from sqlalchemy import event, MetaData, engine_from_config
from sqlalchemy.orm import scoped_session, sessionmaker

from zope.sqlalchemy import ZopeTransactionExtension

from derby.sqla import create_f_unaccent

from pegu.lib.alembic import check_db_version
from pegu.lib.sqla import declarative_bases

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
metadata = MetaData()

event.listen(metadata, 'before_create', create_f_unaccent)

Base, TimestampedBase = declarative_bases(metadata, DBSession)

__all__ = [
    "Grant",
    "City",
    "Funder",
    "User",
    "Instance"
]

from .grants import Grant, City, Funder
from .auth import User
from .instances import Instance

def copy(self):
    """Clone an existing model object."""
    cloned = self.__class__()
    for c in self.__table__.c:
        if c.name == 'id':
            continue
        elif c.name.endswith('ins_date'):
            continue
        elif c.name.endswith('mod_date'):
            continue
        else:
            setattr(cloned, c.name, getattr(self, c.name))
    return cloned


def serialize(self, request):
    vals = dict()
    for c in self.__table__.c:
        val = getattr(self, c.name)
        if isinstance(val, (datetime.date, datetime.datetime)):
            vals[c.name] = val.isoformat()
        else:
            vals[c.name] = val
    return vals


Base.copy = copy
TimestampedBase.copy = copy

Base.__json__ = serialize
TimestampedBase.__json__ = serialize


# Shortcuts
def query(*args, **kwargs):
    return DBSession.query(*args, **kwargs)


def execute(*args, **kwargs):
    return DBSession.execute(*args, **kwargs)


def add(*args, **kwargs):
    return DBSession.add(*args, **kwargs)


def remove(*args, **kwargs):
    return DBSession.remove(*args, **kwargs)


def delete(*args, **kwargs):
    return DBSession.delete(*args, **kwargs)


def flush(*args, **kwargs):
    return DBSession.flush(*args, **kwargs)

def merge(*args, **kwargs):
    return DBSession.merge(*args, **kwargs)


def no_autoflush():
    return DBSession.no_autoflush


def includeme(config):
    """Configure SQLAlchemy, check version at Pyramid app startup time."""
    settings = config.registry.settings
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    check_db_version(engine, settings['config_uri'],
                     settings.get('app.skip_migration'))
