# -*- coding: utf-8 -*-

from sqlalchemy.types import BigInteger, Integer, Unicode, DateTime, Float
from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relationship

from derby.sqla import PGTrigramIndex, pg_search_property

from . import TimestampedBase


class City(TimestampedBase):
    """ Representation of the city of grant """

    __tablename__ = "city"

    name = Column("name", Unicode, primary_key=True)
    province = Column("province", Unicode, primary_key=True)

    lon = Column(Float)
    lat = Column(Float)

    search = pg_search_property(name, search_type="trigram")
    search_province = pg_search_property(province, search_type="trigram")

    __table_args__ = (PGTrigramIndex("city_search_ix", name),
                      PGTrigramIndex("city_province_search_ix", province))


class Grant(TimestampedBase):
    """ Representation of a grant """

    __tablename__ = "grant"

    id = Column(Unicode(24), primary_key=True)
    source = Column(Unicode, index=True)
    code = Column(Unicode(40), index=True)

    funder_id = Column(Unicode(24), ForeignKey('funder.id'))
    name = Column("name", Unicode)
    city = Column("city", Unicode)
    country = Column(Unicode)
    province = Column("province", Unicode)

    date = Column(DateTime, nullable=False)
    end_date = Column(DateTime, nullable=True)
    amount = Column(BigInteger)

    type = Column(Unicode, index=True)
    purpose = Column("purpose", Unicode)
    links = Column(Unicode)
    comments = Column("comments", Unicode)

    lon = Column(Float)
    lat = Column(Float)

    search = pg_search_property(name, search_type="trigram")
    search_full = pg_search_property(purpose, search_type="trigram")
    search_city = pg_search_property(city, search_type="trigram")
    search_province = pg_search_property(province, search_type="trigram")

    __table_args__ = (
        PGTrigramIndex("grant_search_ix", name),
        PGTrigramIndex("grant_search_city_ix", city),
        PGTrigramIndex("grant_search_province_ix", province),
        PGTrigramIndex("grant_search_purpose_ix", purpose))

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return "<Grant: %s>" % self.id

    def __json__(self, request):
        return dict(grant_id=self.id, lon=self.lon, lat=self.lat,
                    name=self.name)


class Funder(TimestampedBase):
    __tablename__ = "funder"

    id = Column(Unicode(24), primary_key=True)
    code = Column(Unicode)
    label = Column(Unicode)
    description = Column(Unicode)
    url = Column(Unicode)
    address_line_1 = Column(Unicode)
    address_line_2 = Column(Unicode)
    province = Column(Unicode)
    postal_code = Column(Unicode)
    country = Column(Unicode)
    grants = relationship("Grant")
