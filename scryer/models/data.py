# encoding: utf-8

"""This module takes care of populating the database."""

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import logging
from random import randint

from ..models import DBSession as db
from . import *  # NOQA

log = logging.getLogger(__name__)


def initial_data():
    """Populate the database with the needed real data."""
    user = User(username='admin', password='test')
    db.add(user)


def sample_data():
    """Populate the database with sample data."""
    pass
