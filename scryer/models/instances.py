import json
import unicodedata
import re

from sqlalchemy.types import BigInteger, Unicode, DateTime, Float, Integer
from sqlalchemy import Column, ForeignKey
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import event

from . import TimestampedBase


class Instance(TimestampedBase):
  __tablename__ = "instance"

  id = Column(Integer, primary_key=True)
  name = Column("name", Unicode, nullable=False)
  slug = Column("slug", Unicode, nullable=False)
  description = Column("description", Unicode)

  # use through .filter getter/setter
  filter_json = Column("filter_json", Unicode)

  @hybrid_property
  def filter(self):
    if self.filter_json:
      return json.loads(self.filter_json)
    return None

  @filter.setter
  def filter(self, value):
    if value:
      self.filter_json = json.dumps(value)

  map_options_json = Column("map_options_json", Unicode)