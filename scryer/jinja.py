# -*- coding: utf-8 -*-

from derby import text
from derby.highlight import highlight_terms

from scryer.api.funder import funder_label


def includeme(config):
    config.include("pyramid_jinja2")

    config.commit()

    config.add_jinja2_search_path("scryer:templates")
    config.include("derby.jinja2")

    jinja_env = config.get_jinja2_environment()

    jinja_env.globals["enumerate"] = enumerate
    jinja_env.globals["sum"] = sum
    jinja_env.globals["funder_label"] = funder_label
    jinja_env.filters["slug"] = text.slugify
    jinja_env.filters["map"] = text.map
    jinja_env.filters["money"] = text.money
    jinja_env.filters["percentage"] = text.percentage
    jinja_env.filters["highlight"] = highlight_terms
