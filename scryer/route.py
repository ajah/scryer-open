# -*- coding: utf-8 -*-


def includeme(config):
    config.add_route("favicon", "/favicon.ico")
    config.add_route("index", "/")
    config.add_route("login", "/login")
    config.add_route("logout", "/logout")
    config.add_route("about", "/about")
    config.add_route("grant.index", "/grants")
    config.add_route("grant.detail", "/grants/{id}")
    config.add_route("grant.table", "/grants-table")
    config.add_route("grant.summary", "/grants-summary")
    config.add_route("grant.data", "/data/")
    config.add_route("grant.donut_data", "/donut-data")
    config.add_route("grant.suggestion", "/grants-suggestion")
    config.add_route("grant.city_suggestion", "/grants-city-suggestion")
    config.add_route("grant.export", "/grants-export")
    config.add_route("grant.modal", "/grants-modal")
    config.add_route("grant.modal_table", "/grants-modal-table")

    config.add_route("funder.detail", "/funder/{id}")

    config.add_route('csv_import.index', '/csv-import')
    config.add_route('csv_import.process', '/csv-import-process')

    config.add_route('instance.instance', '/instance/{slug}/')
    config.add_route('instance.add_instance', '/admin/new/instance/')
    config.add_route('instance.list_instances', '/admin/instance/')
    config.add_route('instance.edit_instance', '/admin/instance/{slug}/')

    config.add_route('instance.delete_instance', '/admin/delete/instance/{slug}/')

