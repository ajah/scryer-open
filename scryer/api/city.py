# -*- coding: utf-8 -*-

from derby.geocode import GeocodingError, GoogleMapsGeocoder


def geocode(city, province=None):
    g = GoogleMapsGeocoder()
    g.ignore_exception = True
    try:
        if province:
            query = "%s, %s (Canada)" % (city, province)
        else:
            query = "%s (Canada)" % city
        results = g.geocode(query)

        if results["longitude"] > -53:
            raise ValueError("Bad longitude")
        if results["longitude"] < -141:
            raise ValueError("Bad longitude")
        if results["latitude"] < 42:
            raise ValueError("Bad latitude")
        if results["latitude"] > 83:
            raise ValueError("Bad latitude")
        return results["longitude"], results["latitude"]
    except GeocodingError:
        return [None, None]
    except AttributeError:
        return [None, None]
    except TypeError:
        return [None, None]
