from scryer import models as m

def get_instance_by_slug(slug):
  return m.query(m.Instance).filter_by(slug=slug).first()