# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

from scryer.lib.province import PROVINCES


def get_provinces():
    """ Get all sources of grants """
    return PROVINCES
