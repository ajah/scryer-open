# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
from scryer import models as m


funders = {
    "ec": "Environment Canada",
    "heritage": "Heritage Canada",
    "hrsdc": "Employment and Social Development Canada (ESDC)",
    "trillium": "Ontario Trillium Foundation",
    "mcconnell": "McConnell Foundation",
    "swc": "Status of Women Canada",
    "justice": "Department of Justice",
    "cic": "Citizenship and Immigration Canada",
    "servicecanada": "Service Canada"
}


def funder_label(code):
    """
    Return the label corresponding to a funder if found or the code
    itself
    """
    return funders.get(code, code)

def get_funder(id_=None):
    """ Get a single funder by its id """
    q = m.query(m.Funder).filter(m.Funder.id == id_)
    return q.one()

def get_funders_grants(id_=None):
    """ Get a funder's grants by its id """
    q = m.query(m.Funder).filter(m.Funder.id == id_)
    funder = q.one()
    return funder.grants
