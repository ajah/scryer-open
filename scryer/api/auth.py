# -*- coding: utf-8 -*-

from scryer import models as m


def get_user(username):
    """ Return a specific user record, if it exists. """
    return m.User.get(username=username)
