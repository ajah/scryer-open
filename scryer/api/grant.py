# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

from string import strip

from sqlalchemy import func, and_, or_ 


from derby.sqla import pg_search_trigram_query

from scryer import models as m


def get_grants(source=None, search="", name="", sort="", city="", province="",
               date_from=None, date_to=None, additional=[]):
    """ Get all grants """
    q = m.query(m.Grant)
    return _filter_grants(q, source, search, name, sort, city, province, date_from, date_to, additional)

def _filter_grants(q, source=None, search="", name="", sort="", city="", province="",
                   date_from=None, date_to=None, additional=[]):
    if source:
        # handle optional comma-separated multi-values
        or_parts = or_(*[m.Grant.source == source_part for source_part in map(strip, source.split(','))])
        q = q.filter(or_parts)

    if additional and not search:
        search = "{0}".format(",".join(additional))
    elif additional:
        search = "{0} {1}".format(search, ",".join(additional))

    search_fields = [[province, 'search_province'],
                      [search, 'search_full'],
                      [name, 'search'],
                      [city, 'search_city']]

    for field_value, model_search_field_name in search_fields:
      if not field_value:
        continue
      or_parts = []
      # handle optional comma-separated multi-values
      for value_part in map(strip, field_value.split(',')):
        arg = pg_search_trigram_query(value_part)
        or_parts.append(getattr(m.Grant, model_search_field_name).op('~')(arg))
      q = q.filter(or_(*or_parts))

    if date_from and date_to:
        q = q.filter(and_(m.Grant.date >= date_from, m.Grant.date <= date_to))
    elif date_from:
        q = q.filter(m.Grant.date >= date_from)
    elif date_to:
        q = q.filter(m.Grant.date <= date_to)

    if "name_up" in sort:
        q = q.order_by(m.Grant.name.asc().nullslast())
    elif "name_down" in sort:
        q = q.order_by(m.Grant.name.desc().nullslast())
    elif "location_up" in sort:
        q = q.order_by(m.Grant.city.asc().nullslast(),
                       m.Grant.province.asc().nullslast())
    elif "location_down" in sort:
        q = q.order_by(m.Grant.city.desc().nullslast(),
                       m.Grant.province.desc().nullslast())
    elif "date_up" in sort:
        q = q.order_by(m.Grant.date.asc().nullslast(),
                       m.Grant.end_date.asc().nullslast())
    elif "date_down" in sort:
        q = q.order_by(m.Grant.date.desc().nullslast(),
                       m.Grant.end_date.desc().nullslast())
    elif "amount_up" in sort:
        q = q.order_by(m.Grant.amount.asc().nullslast())
    elif "amount_down" in sort:
        q = q.order_by(m.Grant.amount.desc().nullslast())
    elif "source_up" in sort:
        q = q.order_by(m.Grant.source.asc().nullslast())
    elif "source_down" in sort:
        q = q.order_by(m.Grant.source.desc().nullslast())
    elif "comments_up" in sort:
        q = q.order_by(m.Grant.comments.asc().nullslast())
    elif "comments_down" in sort:
        q = q.order_by(m.Grant.comments.desc().nullslast())
    elif "purpose_up" in sort:
        q = q.order_by(m.Grant.purpose.asc().nullslast())
    elif "purpose_down" in sort:
        q = q.order_by(m.Grant.purpose.desc().nullslast())

    return q

def get_donut_data(source=None, search="", name="", sort="", city="", province="",
                   date_from=None, date_to=None, additional=[]):
    q = m.query(func.sum(m.Grant.amount), m.Grant.source)
    q = _filter_grants(q, source=source, search=search, name=name,
                       city=city, province=province, date_from=date_from,
                       date_to=date_to, additional=additional)
    return q.group_by(m.Grant.source).all()

def get_grant(id_=None):
    """ Get a single grant by its id """
    q = m.query(m.Grant).filter(m.Grant.id == id_)
    return q.one()


def get_sources():
    """ Get all sources of grants """
    return m.query(m.Grant.source.distinct())


def get_cities(search=""):
    """ Get all city of grants """
    q = m.query(m.Grant)
    if search:
        city_arg = pg_search_trigram_query(search)
        q = q.filter(m.Grant.search_city.op('~')(city_arg))
    return q


def get_year_range():
    assert m.query(func.min(m.Grant.date)).scalar()
    return (m.query(func.min(m.Grant.date)).scalar().year,
            m.query(func.max(m.Grant.date)).scalar().year)


def count_grants(source=None, page=None):
    """ Count grants without any limit and pagination """
    return get_grants(source=source).count()


def get_grants_value(source=None, search="", name="", sort="", city="",
                     province="", date_from=None, date_to=None, additional=[]):
    """ Return the total value of a search query """
    return (get_grants(source=source, search=search, name=name,
                       city=city, province=province, date_from=date_from,
                       date_to=date_to, additional=additional)
            .with_entities(func.sum(m.Grant.amount))
            .scalar())


def get_grants_dates(source=None, search="", name="", sort="", city="",
                     province="", date_from=None, date_to=None, additional=[]):
    """ Return the total value of a search query """
    return (get_grants(source=source, search=search, name=name,
                       city=city, province=province, date_from=date_from,
                       date_to=date_to, additional=additional)
            .with_entities(func.min(m.Grant.date), func.max(m.Grant.date)))

