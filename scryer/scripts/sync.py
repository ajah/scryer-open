# -*- coding: utf-8 -*-


"""Usage:
    scryer-sync gimlet <config> [--year=<year<] [-v] [--source=<source>] [--clear]
    scryer-sync trillium <path> <config> [-v] [--clear]
    scryer-sync mcconnell <path> <config> [-v] [--clear]
    scryer-sync city <path> <config> [-v] [--clear]

Options:
    -v, --verbose        Increase verbosity.
    -h, --help           Print this help.
"""

import json
import logging
import transaction
import unicodecsv

from datetime import datetime
from zope.sqlalchemy import mark_changed
from sqlalchemy.orm.exc import NoResultFound

from bunch import Bunch
from docopt import docopt

from pyramid.paster import bootstrap

from gluten.bindings import Gimlet

from derby.scriptutils import ProgressMeter as PM

from scryer.lib.province import clean_province
from scryer import models as m


log = logging.getLogger(__name__)


class CitySync(object):
    def sync(self, path, verbose=False):
        with open(path) as fileobj:
            items = json.loads(fileobj.read())
            for item in PM.wrap_seq(items, verbose=verbose):
                try:
                    city = (m.query(m.City)
                            .filter(m.City.province == item["province"])
                            .filter(m.City.name == item["name"])
                            .one())
                    if not city.lat:
                        city.lat = item["lat"]
                        city.lon = item["lon"]
                except NoResultFound:
                    city = m.City()
                    city.name = item["name"]
                    city.province = item["province"]
                    city.lat = item["lat"]
                    city.lon = item["lon"]
                    m.add(city)


class McconnellProjectSync(object):
    def __init__(self):
        self.source = "mcconnell"

    def parse_lines(self, path):
        csv_fields = list()
        with open(path) as fileobj:
            dialect = unicodecsv.Sniffer().sniff(fileobj.read(1024))
            fileobj.seek(0)
            csv = unicodecsv.reader(fileobj, dialect)
            csv_fields.extend(map(unicode.strip, csv.next()))
            for i, line in enumerate(csv):
                values = [value for value in line]
                yield Bunch(zip(csv_fields, values) + [('_row', i + 2)])

    def sync(self, path, verbose=False):
        lines = list(self.parse_lines(path))
        for item in PM.wrap_seq(lines, verbose=verbose):
            grant = m.query(m.Grant).filter_by(code=item["code"]).one()
            grant.province = clean_province(item["province"])
            grant.city = item["city"]


class TrilliumProjectSync(object):
    def __init__(self):
        self.source = "trillium"

    def parse_lines(self, path):
        csv_fields = list()
        with open(path) as fileobj:
            dialect = unicodecsv.Sniffer().sniff(fileobj.read(1024))
            fileobj.seek(0)
            csv = unicodecsv.reader(fileobj, dialect)
            csv_fields.extend(map(unicode.strip, csv.next()))
            for i, line in enumerate(csv):
                values = [value for value in line]
                yield Bunch(zip(csv_fields, values) + [('_row', i + 2)])

    def sync(self, path, verbose=False):
        lines = list(self.parse_lines(path))
        for item in PM.wrap_seq(lines, verbose=verbose):
            grant = m.Grant()
            grant.id = item["_row"]
            grant.source = self.source
            try:
                grant.name = item["Organization Name"]
            except KeyError:
                continue
            try:
                grant.city = item["Organization City"]
            except KeyError:
                continue
            grant.country = "Canada"
            try:
                grant.province = clean_province(item["Organization Province"])
            except KeyError:
                continue
            try:
                grant.date = datetime(int(item["Fiscal year of the Decision"]),
                                      1, 1)
            except (ValueError, KeyError):
                continue
            try:
                grant.amount = float(item["Granted Amount$"]
                                     .replace(",", "").replace("$", ""))
            except (ValueError, KeyError):
                continue
            grant.ins_date = datetime.now()
            grant.comments = ""
            grant.purpose = item["Request/Grant Description"]
            m.add(grant)


class GimletProjectSync(object):
    """ Create and sync project from Gimlet """
    DEFAULT_YEAR = 2013

    def __init__(self, user, password, url=None):
        self.api = Gimlet(user, password)
        if url is not None:
            self.api.BASE = url

    def sync(self, year=None, source=None, verbose=False):
        kwargs = dict()
        if source:
            kwargs["source"] = source
        if year:
            kwargs["year"] = year

        results = self.api.grant.get_all(**kwargs)
        count = self.api.grant.count(**kwargs)

        for gift in PM.wrap_seq(results, nb_item=count,
                                verbose=verbose):
            try:
                m.query(m.Grant).filter_by(id=gift.id).one()
            except NoResultFound:
                print("adding a new grant of %s from %s with a value of %s" % (
                      gift.name,
                      gift.source,
                      gift.amount))
                grant = m.Grant()
                grant.id = gift.id
                grant.source = gift.source
                grant.code = gift.code
                grant.name = gift.name
                grant.city = gift.city
                grant.country = gift.country
                if gift.state:
                    grant.province = clean_province(gift.state)
                else:
                    grant.province = clean_province(gift.province)
                if gift.date:
                    grant.date = gift.date
                if gift.end_date:
                    grant.end_date = gift.end_date
                try:
                    grant.amount = float(gift.amount)
                except ValueError:
                    pass
                grant.ins_date = gift.timestamp
                for details in gift.details:
                    if details.lang == "en":
                        grant.type = details.type
                        grant.purpose = details.purpose
                        grant.links = details.link
                        grant.comments = details.comments
                m.add(grant)
            else:
                print("grant already exist %s from %s with a value of %s" % (
                      gift.name,
                      gift.source,
                      gift.amount))


def main():
    args = docopt(__doc__)
    env = bootstrap(args["<config>"])
    settings = env["registry"].settings
    user = settings["gimlet.username"]
    password = settings["gimlet.password"]
    url = settings["gimlet.url"]

    s = m.DBSession()
    with transaction.manager:
        if args["--clear"]:
            q = m.Grant.__table__.delete()
            if args["--source"]:
                q = q.where(m.Grant.__table__.c.source == args["--source"])
            elif args["trillium"]:
                q = q.where(m.Grant.__table__.c.source == "trillium")
            m.execute(q)
            mark_changed(s)
        if args["gimlet"]:
            (GimletProjectSync(user, password, url)
             .sync(args["--year"],
                   args["--source"],
                   args["--verbose"]))
        elif args["trillium"]:
            TrilliumProjectSync().sync(args["<path>"], args["--verbose"])
        elif args["mcconnell"]:
            McconnellProjectSync().sync(args["<path>"], args["--verbose"])
        elif args["city"]:
            CitySync().sync(args["<path>"], args["--verbose"])
    env["closer"]()
