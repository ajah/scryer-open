# -*- coding: utf-8 -*-

"""Usage:
    scryer-geo [-v] [--clear] [-s] [--source=<source>] <config>

Options:
    -s, --sleep          Enable sleep.
    -v, --verbose        Increase verbosity.
    -h, --help           Print this help.
"""

import logging
import transaction

from time import sleep
from docopt import docopt
from pyramid.paster import bootstrap

from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import not_

from derby.scriptutils import ProgressMeter as PM

from scryer import models as m
from scryer.api.city import geocode


log = logging.getLogger(__name__)


def main():
    args = docopt(__doc__)
    bootstrap(args["<config>"])

    q = m.query(m.Grant).filter(not_(m.Grant.city.is_(None)))

    if args["--source"]:
        q = q.filter(m.Grant.source == args["--source"])

    if not args["--clear"]:
        q = q.filter(m.Grant.lon.is_(None))
    else:
        q = q.filter(not_(m.Grant.lon.is_(None)))

    for i, grant in enumerate(PM.wrap_seq(q, verbose=args["--verbose"])):
        grant = m.merge(grant)

        if grant.province is None:
            continue
        if grant.city is None:
            continue
        city = None
        try:
            city = (m.query(m.City)
                    .filter_by(name=grant.city)
                    .filter_by(province=grant.province)
                    .one())
        except NoResultFound:
            city = m.City()
            city.province = grant.province
            city.name = grant.city
            m.add(city)
        try:
            if args["--clear"] or city.lon is None:
                lon, lat = geocode(city=city.name, province=city.province)
                city.lon = lon
                city.lat = lat
            grant.lon = city.lon
            grant.lat = city.lat
            if args["--verbose"]:
                print("%s (%s) is at %s %s" % (city.name, city.province,
                                               city.lon, city.lat))
        except ValueError:
            if args["--verbose"]:
                print("Can't find %s (%s)" % (city.name, city.province))
        if i % 100:
            transaction.commit()
        if args["--sleep"]:
            sleep(2)
    transaction.commit()
