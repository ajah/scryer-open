# -*- coding: utf-8 -*-

"""Usage:
    scryer-export [-v] mcconnel <path> <config>
    scryer-export [-v] geo <path> <config>

Options:
    -v, --verbose        Increase verbosity.
    -h, --help           Print this help.
"""

import logging
import unicodecsv as csv
import json

from os.path import dirname, join

from docopt import docopt

from sqlalchemy import not_

from pyramid.paster import bootstrap

from derby.scriptutils import ProgressMeter as PM

from scryer import resources
from scryer import models as m

log = logging.getLogger(__name__)


def main():
    args = docopt(__doc__)
    bootstrap(args["<config>"])

    if args["mcconnel"]:
        path = join(dirname(resources.__file__), "mcconnell.json")

        with open(path) as source:
            with open(args["<path>"], "w") as dest:
                fd = csv.writer(dest, encoding='utf-8')
                for i, item in enumerate(json.loads(source.read())):
                    if i == 0:
                        fd.writerow(item.keys())
                    fd.writerow(item.values())
    elif args["geo"]:
        vals = []
        q = m.query(m.City).filter(not_(m.City.lon.is_(None)))
        for i, city in enumerate(PM.wrap_seq(q, verbose=args["--verbose"])):
            vals.append({
                "name": city.name,
                "province": city.province,
                "lon": city.lon,
                "lat": city.lat
            })
        with open(args["<path>"], "w") as fd:
            fd.write(json.dumps(vals))
