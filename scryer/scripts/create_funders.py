# -*- coding: utf-8 -*-

"""Usage:
    scryer-create-funders [-v] [-c CONFIG]

Options:
    -c, --config=CONFIG  Paste config to use. [default: local.ini]
    -v, --verbose        Increase verbosity.
    -h, --help           Print this help.
"""

import logging
import uuid
import transaction

from docopt import docopt
from sqlalchemy import not_
from pyramid.paster import bootstrap

from scryer import resources
from scryer import models as m
from scryer.api.funder import funder_label

log = logging.getLogger(__name__)


def main():
    args = docopt(__doc__)
    env = bootstrap(args["--config"])

    grants = m.query(m.Grant).all()

    for grant in grants:
        grant = m.merge(grant)

        if not grant.funder_id:
            funder = m.query(m.Funder).filter(m.Funder.code == grant.source).first()
             
            if not funder:
                funder = m.Funder()

                funder_id = str(uuid.uuid4()).replace('-','')[:24]
                funder.id = funder_id
                funder.code = grant.source
                funder.label = funder_label(grant.source)

                m.add(funder)
                transaction.commit()
            else:
                funder = m.merge(funder)
                funder_id = funder.id

            grant.funder_id = funder_id
            m.add(grant)
            transaction.commit()

