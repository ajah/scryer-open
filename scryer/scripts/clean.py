# -*- coding: utf-8 -*-


"""Usage:
    scryer-clean province [-c CONFIG] [-v]

Options:
    -c, --config=CONFIG  Paste config to use. [default: local.ini]
    -v, --verbose        Increase verbosity.
    -h, --help           Print this help.
"""

import logging
import transaction

from docopt import docopt

from pyramid.paster import bootstrap

from derby.scriptutils import ProgressMeter as PM

from scryer.lib.province import clean_province
from scryer import models as m


log = logging.getLogger(__name__)


def main():
    args = docopt(__doc__)
    env = bootstrap(args["--config"])
    verbose = args["--verbose"]
    with transaction.manager:
        if args["province"]:
            for item in PM.wrap_seq(m.query(m.Grant), verbose=verbose):
                item.province = clean_province(item.province)
    env["closer"]()
