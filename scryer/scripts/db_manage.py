"""Database management utilities and alembic wrapper."""

from __future__ import (absolute_import, division, print_function,
        unicode_literals)

from contextlib import contextmanager
from os import path
from urlparse import urlparse

from opster import command, dispatch
from pyramid.paster import bootstrap as bootstrap_pyramid

from pegu.lib.alembic import bootstrap as bootstrap_alembic, run_command
from pegu.lib.database import (backup as backup_db, restore as restore_db,
                               wipe as wipe_db)
from pegu.lib.sqla import pyramid_create_engine

from ..models import DBSession, metadata, User, add as add_db

import transaction

SCRIPT_NAME = 'caneom-db'
DEFAULT_CONFIG = 'local.ini'
OPTIONS = [('c', 'config', DEFAULT_CONFIG, 'configuration file')]


def alembic(force=('f', False, 'run even if database is not initialized'),
            ahelp=('a', False, 'show alembic command usage'),
            *args, **kwargs):
    config = kwargs.pop('config')
    if ahelp:
        args = list(args) + ['--help']
    print(run_command(config, DBSession, ahelp or force, *args, **kwargs))
alembic.__doc__ = run_command.__doc__
alembic = command(usage='[ALEMBIC_OPTS...]')(alembic)


def bootstrap(wipe=('w', False, 'wipe the database beforehand'),
              empty=('e', False, 'skip initial data'),
              sample=('s', False, 'include sample data'), **kwargs):
    config = kwargs.pop('config')
    if wipe:
        wipe_db(DBSession, config)
    bootstrap_alembic(DBSession, metadata, config, empty=empty, sample=sample)
bootstrap.__doc__ = bootstrap_alembic.__doc__
bootstrap = command()(bootstrap)


def backup(**kwargs):
    with pyramid_dbname(kwargs['config']) as (settings, dbname):
        filename = path.join(settings['app.data_dir'], 'pg-%s.sql' % dbname)
        backup_db(dbname, filename)
backup.__doc__ = backup_db.__doc__
backup = command()(backup)


def restore(filename=('f', None, 'filename of the data dump'), **kwargs):
    with pyramid_dbname(kwargs['config']) as (settings, dbname):
        if not filename:
            filename = 'pg-%s.sql.bz2' % dbname
        filename = path.join(settings['app.data_dir'], filename)
        print(restore_db(dbname, filename))
restore.__doc__ = restore_db.__doc__
restore = command()(restore)


def wipe(**kwargs):
    config = kwargs.pop('config')
    wipe_db(DBSession, config)
wipe.__doc__ = wipe_db.__doc__
wipe = command()(wipe)


@command()
def createuser(username, password, **kwargs):
    """Create a user in the database."""
    config = kwargs.pop('config')
    engine = pyramid_create_engine(config)
    DBSession.configure(bind=engine)
    user_obj = User(username=username, password=password)
    add_db(user_obj)
    transaction.commit()


@contextmanager
def pyramid_app(config):
    env = bootstrap_pyramid(config)
    yield env['registry'].settings

    env['closer']()


@contextmanager
def pyramid_dbname(config):
    with pyramid_app(config) as settings:
        dbconf = urlparse(settings['sqlalchemy.url'])
        if dbconf.scheme != 'postgres' or not dbconf.path.startswith('/'):
            raise ValueError('Invalid DB configuration %r' % dbconf.geturl())

        yield settings, dbconf.path[1:]


def main():
    dispatch(globaloptions=OPTIONS)


if __name__ == '__main__':
    main()
