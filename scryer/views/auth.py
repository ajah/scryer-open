# -*- coding: utf-8 -*-

from base64 import b64decode

from pyramid.view import view_config
from pyramid.security import remember, forget
from pyramid.httpexceptions import HTTPFound

from scryer.api.auth import get_user


@view_config(route_name='login', renderer='login.jinja2')
def login(request):
    redirect = request.params.get("redirect", None)
    return dict(redirect=redirect)


@view_config(route_name='logout', renderer='login.jinja2')
def logout(request):
    headers = forget(request)
    login_redirect = request.route_path("login")
    raise HTTPFound(location=request.route_path("index"),
                    headers=headers)


@view_config(route_name='login', renderer='login.jinja2',
             request_method="POST")
def do_login(request):
    username = request.params.get("user", None)
    password = request.POST.get("password", None)
    redirect = request.params.get("redirect") or request.route_path("index")
    login_redirect = request.route_path("login")

    if username and password:
        user = get_user(username)
        if user and user.verify_password(password):
            headers = remember(request, username)
            return HTTPFound(location=redirect,
                             headers=headers)
    raise HTTPFound(location=login_redirect)
