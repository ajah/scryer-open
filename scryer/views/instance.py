import transaction
from datetime import datetime

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from pyramid.response import Response
from pyramid.httpexceptions import HTTPNotFound

from scryer.api.province import get_provinces
from scryer.api.grant import (get_grants, get_sources, get_year_range,
                              get_donut_data)
from scryer.api.instance import get_instance_by_slug
from scryer import models as m
from scryer.models import DBSession


@view_config(route_name='instance.instance',
              renderer='grant.jinja2')
def instance(request):
  slug = request.matchdict["slug"]
  instance = get_instance_by_slug(slug)
  if not instance:
    raise HTTPNotFound

  start_year, end_year = get_year_range()
  year_range = range(start_year, end_year + 1)

  # TODO taken from views/grant --- any way to make it DRYer?
  args = dict(
    date_from = int(request.GET.get("date_from", "0") or "0"),
    date_to = int(request.GET.get("date_to", "0") or "0"),

    page = int(request.GET.get("page", "1") or "1"),
    limit = int(request.GET.get("limit", "100") or "100"),

    source = request.GET.get("source", ""),
    name = request.GET.get("name", ""),
    sort = request.GET.get("sort", ""),
    city = request.GET.get("city", ""),
    province = request.GET.get("province", ""),
    search = request.GET.get("search", ""),

    sources = get_sources(),
    provinces = get_provinces(),

    start_year = start_year,
    end_year = end_year,
    year_range = year_range,

    hidden_filter_fields = [],
    map_options = instance.map_options_json,
    instance=instance
   )

  # override GET args with given instance filter values
  field_mapper = {
    "start_year": "date_from",
    "end_year": "date_to",
  }

  for field_name, field in instance.filter.items():
    args[field_mapper.get(field_name, field_name)] = field['value']
    if field['hide']:
      args['hidden_filter_fields'].append(field_name)

  donut_data_ = get_donut_data(args["source"], args["search"], args["name"],
                               args["sort"], args["city"], args["province"],
                               datetime(args["start_year"], 1, 1, 23, 59, 59),
                               datetime(args["end_year"], 12, 31, 23, 59, 59))
  total = sum([d[0] for d in donut_data_])
  donut_data = {}
  for d in donut_data_:
      donut_data[d[1]] = d[0] / total
  args["donut_data"] = donut_data

  return dict(**args)

def _save_instance(request=None, instance=None, is_new=None):
  error = None

  name = request.POST.get('name')
  slug = request.POST.get('slug')
  description = request.POST.get('description')
  map_options_json = request.POST.get('mapOptions')

  FIELDS = ['name', 'city', 'province', 'source', 'search', 'start_year',
            'end_year', 'search', 'city']

  field_values = {}
  for f in FIELDS:
    val = request.POST.get('filter_%s' % f)
    if not val:
      continue
    field_values[f] = {
      'value': val,
      'hide': request.POST.get('hide_%s' % f) == 'on'
    }

  if not name:
    error = 'No name provided'    
  elif not description:
    error = 'No description provided'
  elif not field_values:
    error = 'No data provided for any of the fields'
  elif not slug:
    error = 'No slug provided'
  elif is_new and get_instance_by_slug(slug) is not None:
    error = 'This slug is already used. Please use another slug.'
  else:
    i = instance
    i.name = name
    i.slug = slug
    i.description = description
    i.map_options_json = map_options_json

    i.filter = field_values

    if is_new:
      m.add(i)
    else:
      i.save()

    transaction.commit()

  return {'error': error}


@view_config(route_name='instance.add_instance', renderer='instance-admin.jinja2',
              permission='private')
def add_instance(request):
  error = None
  if request.method == 'POST':
      instance = m.Instance()
      res = _save_instance(request=request, instance=instance, is_new=True)
      if res['error'] is None:
        raise HTTPFound(location=request.route_path("instance.list_instances"))
      else:
        error = res['error']

  sources = get_sources().all(),
  return {'instance': None,
          'action': request.route_path('instance.add_instance'),
          'sources': sources,
          'error': error}


@view_config(route_name='instance.delete_instance', renderer='instance-delete.jinja2',
              permission='private')
def delete_instance(request):
  error = None
  slug = request.matchdict['slug']
  instance = get_instance_by_slug(slug)
  if not instance:
        raise HTTPNotFound

  if request.method == 'POST':
      instance.delete()
      transaction.commit()
      raise HTTPFound(location=request.route_path('instance.list_instances'))

  return {'instance': instance,
          'action': request.route_path('instance.add_instance'),
          'error': error}

@view_config(route_name='instance.edit_instance', renderer='instance-admin.jinja2',
              permission='private')
def edit_instance(request):
  slug = request.matchdict['slug']
  instance = get_instance_by_slug(slug)

  if not instance:
    raise HTTPNotFound

  error = None
  if request.method == 'POST':
    id = request.POST.get('instanceId')
    instance = m.Instance.get(id=id)
    
    # should not happen
    if not instance:
      raise HTTPFound

    res = _save_instance(request=request, instance=instance, is_new=False)
    if not res['error']:
      raise HTTPFound(location=request.route_path('instance.list_instances'))
    else:
      res['error'] = error

  sources = get_sources().all(),
  return {'instance': instance,
          'action': request.route_path('instance.edit_instance', slug=instance.slug),
          'sources': sources,
          'error': error}

@view_config(route_name='instance.list_instances', renderer='instance-list.jinja2',
              permission='private')
def list_instances(request):
  instances = m.query(m.Instance).order_by(m.Instance.mod_date.desc()).all()
  return {'instances': instances}
