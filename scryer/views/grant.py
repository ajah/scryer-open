# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

from datetime import datetime
from pyramid.view import view_config
from webhelpers.paginate import Page

from scryer import models as m

from scryer.lib.keyword import get_default_keyword

from scryer.api.province import get_provinces
from scryer.api.grant import (get_grants, get_grants_value, get_grants_dates,
                              get_sources, get_cities, get_year_range,
                              get_grant, get_donut_data)
from scryer.api.funder import funder_label, get_funder, get_funders_grants



@view_config(route_name="grant.index", renderer="grant.jinja2")
def grant(request):
    date_from = int(request.GET.get("date_from", "0") or "0")
    date_to = int(request.GET.get("date_to", "0") or "0")

    page = int(request.GET.get("page", "1") or "1")
    limit = int(request.GET.get("limit", "100") or "100")

    source = request.GET.get("source", "")
    name = request.GET.get("name", "")
    sort = request.GET.get("sort", "")
    city = request.GET.get("city", "")
    province = request.GET.get("province", "")
    search = request.GET.get("search", "")
    sources = get_sources()
    provinces = get_provinces()

    start_year, end_year = get_year_range()
    year_range = range(start_year, end_year + 1)

    donut_data_ = get_donut_data(source, search, name, sort, city, province,
                                 datetime(start_year, 1, 1, 23, 59, 59),
                                 datetime(end_year, 12, 31, 23, 59, 59))
    total = sum([d[0] for d in donut_data_])
    donut_data = {}
    for d in donut_data_:
        donut_data[d[1]] = d[0] / total

    return dict(source=source, province=province, page=page, limit=limit,
                search=search, sources=sources, provinces=provinces, name=name,
                sort=sort, city=city, start_year=start_year, end_year=end_year,
                year_range=year_range, date_from=date_from, date_to=date_to,
                hidden_filter_fields=[], map_options={}, donut_data=donut_data)


@view_config(route_name="grant.donut_data", renderer="json")
def grant_donut_data(request):
    source = request.GET.get("source", "")
    name = request.GET.get("name", "")
    sort = request.GET.get("sort", "")
    city = request.GET.get("city", "")
    province = request.GET.get("province", "")
    search = request.GET.get("search", "")

    sources = get_sources()
    provinces = get_provinces()

    start_year, end_year = get_year_range()

    donut_data_ = get_donut_data(source, search, name, sort, city, province,
                                 datetime(start_year, 1, 1, 23, 59, 59),
                                 datetime(end_year, 12, 31, 23, 59, 59))
    total = sum([d[0] for d in donut_data_])
    donut_data = {}
    for d in donut_data_:
        donut_data[funder_label(d[1])] = float(d[0]) / float(total)

    return dict(donut_data=donut_data)


@view_config(route_name="grant.detail", renderer="grant-detail.jinja2")
def grant_detail(request):
    id_ = request.matchdict['id']
    grant = get_grant(id_)

    return dict(grant=grant)

@view_config(route_name="funder.detail", renderer="funder-detail.jinja2")
def funder_detail(request):
    limit = int(request.GET.get("limit", "100") or "100")
    page = int(request.GET.get("page", "1") or "1")
    id_ = request.matchdict['id']
    funder = get_funder(id_)
    grants = get_funders_grants(id_)

    def gen_url(*args, **kwargs):
        page = int(kwargs.get("page", "1"))
        query = {"page": page}

        return request.route_path("funder.detail", id=id_, _query=query)

    pager = Page(grants, page=page, items_per_page=limit, url=gen_url)

    return dict(funder=funder, grants=grants, pager=pager)

@view_config(route_name="grant.summary", renderer="grant-summary.jinja2")
def grant_summary(request):
    date_from = int(request.GET.get("date_from", "0") or "0")
    date_to = int(request.GET.get("date_to", "0") or "0")

    source = request.GET.get("source", "")
    name = request.GET.get("name", "")
    sort = request.GET.get("sort", "")
    city = request.GET.get("city", "")
    province = request.GET.get("province", "")
    search = request.GET.get("search", "")

    if date_from:
        date_from = datetime(date_from, 1, 1)

    if date_to:
        date_to = datetime(date_to, 12, 31)

    keywords = get_default_keyword(request)

    grants = get_grants(source=source, search=search, name=name, sort=sort,
                        city=city, province=province, date_from=date_from,
                        date_to=date_to, additional=keywords)
    value = get_grants_value(source=source, search=search, name=name,
                             sort=sort, city=city, province=province,
                             date_from=date_from, date_to=date_to,
                             additional=keywords)
    dates = get_grants_dates(source=source, search=search,
                             name=name, sort=sort, city=city,
                             province=province, date_from=date_from,
                             date_to=date_to, additional=keywords)
    min_date, max_date = dates.all()[0]

    return dict(source=source, province=province, name=name,
                grants_count=grants.count(), value=value, max_date=max_date,
                min_date=min_date, date_from=date_from, date_to=date_to)


@view_config(route_name="grant.data", renderer="json")
def grant_data(request):

    limit = 1000000

    page = int(request.POST.get("page", "1") or "1")
    date_from = int(request.POST.get("date_from", "0") or "0")
    date_to = int(request.POST.get("date_to", "0") or "0")

    source = request.POST.get("source", "")
    name = request.POST.get("name", "")
    city = request.POST.get("city", "")
    province = request.POST.get("province", "")
    search = request.POST.get("search", "")

    if date_from:
        date_from = datetime(date_from, 1, 1)

    if date_to:
        date_to = datetime(date_to, 12, 31)

    keywords = get_default_keyword(request)

    grants = get_grants(source=source, search=search, name=name, city=city,
                        province=province, date_from=date_from,
                        date_to=date_to, additional=keywords)
    pager = Page(grants, page=page, items_per_page=limit)
    return dict(pager=pager)


@view_config(route_name="grant.table", renderer="grant-table.jinja2")
def grant_table(request):
    limit = int(request.GET.get("limit", "100") or "100")
    page = int(request.GET.get("page", "1") or "1")
    initial_date_from = int(request.GET.get("date_from", "0") or "0")
    initial_date_to = int(request.GET.get("date_to", "0") or "0")

    source = request.GET.get("source", "")
    name = request.GET.get("name", "")
    sort = request.GET.get("sort", "")
    city = request.GET.get("city", "")
    province = request.GET.get("province", "")
    search = request.GET.get("search", "")
    instance_slug = request.GET.get("instance_slug", "")

    if initial_date_from:
        date_from = datetime(initial_date_from, 1, 1)
    else:
        date_from = initial_date_from

    if initial_date_to:
        date_to = datetime(initial_date_to, 12, 31)
    else:
        date_to = initial_date_to

    keywords = get_default_keyword(request)
    def gen_url(*args, **kwargs):
        page = int(kwargs.get("page", "1"))
        query = {"page": page, "source": source, "limit": limit,
                 "search": search, "name": name, "sort": sort,
                 "city": city, "province": province, "date_from": initial_date_from,
                 "date_to": initial_date_to, "instance_slug": instance_slug}

        if instance_slug:
            return request.route_path("instance.instance", slug=instance_slug, _query=query)
        else:
            return request.route_path("grant.index", _query=query)


    grants = get_grants(source=source, search=search, name=name, sort=sort,
                        city=city, province=province, date_from=date_from,
                        date_to=date_to, additional=keywords)
    pager = Page(grants, page=page, items_per_page=limit,
                 url=gen_url)
    terms = search.split()

    return dict(pager=pager, terms=terms)


@view_config(route_name="grant.suggestion", renderer="json")
def grant_suggestion(request):
    search = request.params.get("query", "")
    source = request.params.get("source", "")
    name = request.params.get("name", "")
    limit = int(request.params.get("limit", "9"))
    q = get_grants(source=source, search=search, name=name)
    q = q.limit(limit).distinct(m.Grant.name)
    suggestions = [{"value": grant.name, "data": grant.name}
                   for grant in q]
    return dict(query=search,
                suggestions=suggestions)


@view_config(route_name="grant.city_suggestion", renderer="json")
def grant_city_suggestion(request):
    search = request.params.get("query", "")
    limit = int(request.params.get("limit", "9"))
    q = get_cities(search=search)
    q = q.limit(limit).distinct(m.Grant.city)
    suggestions = [{"value": g.city, "data": g.city}
                   for g in q]
    return dict(query=search,
                suggestions=suggestions)


@view_config(route_name="grant.export", renderer="csv")
def grant_export(request):
    def extract_columns(g):
        return (g.id, g.source, g.code, g.name, g.city, g.country, g.province,
                g.date, g.end_date, g.amount, g.type, g.purpose, g.links,
                g.comments)
    source = request.params.get("source", "")
    search = request.params.get("search", "")
    name = request.params.get("name", "")
    sort = request.params.get("sort", "")
    city = request.params.get("city", "")
    province = request.params.get("province", "")

    date_from = int(request.params.get("date_from") or "0")
    date_to = int(request.params.get("date_to") or "0")

    if date_from:
        date_from = datetime(date_from, 1, 1)

    if date_to:
        date_to = datetime(date_to, 12, 31)

    keywords = get_default_keyword(request)

    header = ["id",
              "source",
              "code",
              "name",
              "city",
              "country",
              "province",
              "date",
              "end_date",
              "amount",
              "type",
              "purpose",
              "links",
              "comments"]

    grants = get_grants(source=source, search=search, name=name, sort=sort,
                        city=city, province=province, date_from=date_from,
                        date_to=date_to, additional=keywords)

    filename = "grants.csv"
    rows = [extract_columns(g) for g in grants]

    return dict(header=header, rows=rows, filename=filename)


@view_config(route_name="grant.modal", renderer="grant-modal.jinja2")
def grant_modal(request):
    return dict()


@view_config(route_name="grant.modal_table",
             renderer="grant-modal-table.jinja2")
def grant_modal_table(request):
    grants = (m.query(m.Grant)
              .filter(m.Grant.id.in_(request.POST.getall("ids[]"))))
    return dict(grants=grants)
