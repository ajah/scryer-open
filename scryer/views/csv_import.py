import csv
import uuid
import transaction
from datetime import datetime
from dateutil.parser import parse

from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response

from scryer import models as m

class ImportException(Exception):
  pass

@view_config(route_name='csv_import.index', renderer='csv-import-index.jinja2',
            permission='private')
def index(request):
  if request.method == 'POST':
    try:
      imported_rows = _import_file(request.POST['csv'].file)
    except ImportException as e:
      return {'error': e.message}

    return Response('%d rows imported' % imported_rows)

  return {}

def _import_file(f):
  try:
    data = list(csv.DictReader(f))
  except:
    raise ImportException('Could not read file')

  # if no rows or not more than 1 column, abort
  assert len(data), ImportException('No rows found')
  assert len(data[0]) > 1, ImportException('No columns found')

  grant_cols = m.Grant.__table__.columns.keys()

  csv_col_to_model_col = lambda _: _.lower().replace(' ', '_')
  def csv_val_to_model_col(col, val):
    if col in ['date', 'end_date']:
      return parse(val)
    return val

  mandatory_cols = ['date', 'end_date']
  errors = []
  imported_rows = 0

  for row_idx, row in enumerate(data):
    g = m.Grant()
    # following present data-scheme -- generating 24-byte hex random id
    g.id = str(uuid.uuid4()).replace('-','')[:24]

    for col, val in row.items():
      col = csv_col_to_model_col(col)
      val = csv_val_to_model_col(col, val)

      if not col in grant_cols:
        continue

      setattr(g, col, val)

    for col in mandatory_cols:
      if getattr(g, col) is None:
        errors.append("Mandatory '%s' column not found in row %d" % (col, row_idx))
        continue

    m.add(g)
    imported_rows += 1

  transaction.commit()
  return imported_rows