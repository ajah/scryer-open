# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division, print_function,
        unicode_literals)

import os
import scryer

from pyramid.httpexceptions import HTTPNotFound, HTTPFound, HTTPForbidden
from pyramid.response import FileResponse
from pyramid.view import view_config

from scryer.views.grant import grant


@view_config(route_name='favicon')
def favicon_view(request):
    root = os.path.dirname(scryer.__file__)
    static = request.registry.settings['app.static.url']
    icon = os.path.join(root, static, 'favicon.png')
    return FileResponse(icon, request=request)


@view_config(route_name="index", renderer="grant.jinja2")
def index(request):
    return grant(request)


@view_config(context=HTTPForbidden)
def redirect_forbidden(request):
    """Redirect forbidden requests to login form."""
    return HTTPFound(location=request.route_path("login",
                                _query=[('redirect', request.path_qs)]))


@view_config(context=HTTPNotFound, renderer="404.jinja2")
def not_found_handler(request):
    return {}


@view_config(route_name="about", renderer="about.jinja2")
def about(request):
    return {}
