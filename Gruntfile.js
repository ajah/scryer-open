module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        datetime: Date.now(),
        concat: {
            'scryer-js': {
                src: [
                    'bower_components/leaflet/index.js',
                    'bower_components/jquery/jquery.js',
                    'bower_components/jquery.rest/dist/1/jquery.rest.js',
                    'bower_components/history.js/scripts/bundled/html4+html5/jquery.history.js',
                    'bower_components/bootstrap/dist/js/bootstrap.js',
                    'bower_components/flight/index.js',
                    'bower_components/devbridge-autocomplete/dist/jquery.autocomplete.js',
                    'bower_components/leaflet-providers/leaflet-providers.js',
                    'bower_components/Leaflet.awesome-markers/dist/leaflet.awesome-markers.js',
                    'bower_components/leaflet.markercluster/dist/leaflet.markercluster-src.js',
                    'bower_components/mapbox/dist/mapbox.js'
                ],
                dest: 'scryer/static/js/scryer.js'
            }
        },
        uglify: {
            'options': {
                mangle: {toplevel: true},
                squeeze: {dead_code: false},
                codegen: {quote_keys: true}
            },
            'scryer-js': {
                src: 'scryer/static/js/scryer.js',
                dest: 'scryer/static/js/scryer.min.js'
            }
        }
    });


    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

     grunt.registerTask('default', [
        'concat:scryer-js',
        'uglify:scryer-js'
    ]);
}
