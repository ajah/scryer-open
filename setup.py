import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, "README.md")).read()
CHANGES = open(os.path.join(here, "CHANGES.txt")).read()

requires = [
    "Babel==2.2.0",
    "Beaker==1.8.0",
    "GitPython==1.0.2",
    "Jinja2==2.8",
    "Mako==1.0.4",
    "MarkupSafe==0.23",
    "PasteDeploy==1.5.2",
    "Pillow==2.4.0",
    "PyYAML==3.11",
    "Pygments==2.1.3",
    "SQLAlchemy==0.8.7",
    "SocksiPy-branch==1.01",
    "WebHelpers==1.3",
    "WebOb==1.6.0",
    "alembic==0.8.5",
    "argparse==1.2.1",
    "arrow==0.7.0",
    "backports-abc==0.4",
    "backports.ssl-match-hostname==3.5.0.1",
    "bumpversion==0.5.3",
    "bunch==1.0.1",
    "certifi==2016.2.28",
    "cffi==1.5.2",
    "chaussette==1.3.0",
    "circus==0.13.0",
    "contextlib2==0.5.1",
    "cryptography==1.3.1",
    "dateutils==0.6.6",
    "decorator==4.0.9",
    "desub==1.0.3",
    "docopt==0.6.2",
    "dogpile.cache==0.5.7",
    "dogpile.core==0.4.1",
    "ecdsa==0.13",
    "enum34==1.1.2",
    "funcsigs==0.4",
    "future==0.15.2",
    "gitdb==0.6.4",
    "htpasswd==2.3",
    "idna==2.1",
    "iowait==0.2",
    "ipaddress==1.0.16",
    "keepassdb==0.2.1",
    "markdown2==2.3.1",
    "nltk==3.0.0",
    "opster==4.1",
    "orderedmultidict==0.7.6",
    "paramiko==1.16.0",
    "passlib==1.6.5",
    "pathlib==1.0.1",
    "psutil==4.1.0",
    "psycopg2==2.6.1",
    "pyOpenSSL==16.0.0",
    "pyScss==1.3.4",
    "pyasn1==0.1.9",
    "pycparser==2.14",
    "pycrypto==2.6.1",
    "pymiproxy==1.0",
    "pyramid==1.6.1",
    "pyramid-beaker==0.8",
    "pyramid-debugtoolbar==2.4.2",
    "pyramid-jinja2==2.6.2",
    "pyramid-mako==1.0.2",
    "pyramid-tm==0.12.1",
    "python-dateutil==2.5.2",
    "python-editor==1.0",
    "pytz==2016.3",
    "pyzmq==15.2.0",
    "raven==5.12.0",
    "repoze.lru==0.6",
    "requests==2.9.1",
    "setuptools>11.3",
    "simplejson==3.8.2",
    "singledispatch==3.4.0.3",
    "six==1.10.0",
    "smmap==0.9.0",
    "tornado==4.3",
    "transaction==1.4.4",
    "translationstring==1.3",
    "unicodecsv==0.14.1",
    "venusian==1.0",
    "waitress==0.9.0b0",
    "wsgiref==0.1.2",
    "zope.deprecation==4.1.2",
    "zope.interface==4.1.3",
    "zope.sqlalchemy==0.7.6",
]

setup(
    name="scryer",
    version="1.3.1",
    description="Ajah Project Browser",
    long_description=README + "\n\n" + CHANGES,
    classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
    ],
    author="",
    author_email="",
    url="",
    keywords="web wsgi bfg pylons pyramid",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    test_suite="scryer",
    install_requires=requires,
    entry_points="""\
        [console_scripts]
        scryer-clean = scryer.scripts.clean:main
        scryer-db = scryer.scripts.db_manage:main
        scryer-export = scryer.scripts.export:main
        scryer-geo = scryer.scripts.geolocalize:main
        scryer-sync = scryer.scripts.sync:main
        scryer-create-funders = scryer.scripts.create_funders:main
        [paste.app_factory]
        main = scryer:main
    """,
    message_extractors={".": [
        ("**.py", "lingua_python", None),
        ("**.jinja2", "jinja2", None)
    ]},
)
