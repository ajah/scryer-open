scryer-open
======
The open-sourced code of scryer AKA Landscape. You can view the site by going to the main [LANDSCAPE site](http://landscape.ajah.ca).

The code is available under the MIT License. You can view the [LICENSE file](https://bitbucket.org/ajah/scryer-open/src/f86e2621e4ee96b3bb147a38b282b4e2417274ba/LICENSE.md?at=master&fileviewer=file-view-default) to learn more.

Below are instructions for running the service. Some aspects may not function, as they depend on proprietary software already written and maintained by Ajah.

Getting Started
---------------

You have to follow the following options to setup your instance:

    # OSX-specific
    $ brew install postgresql

On the Python side:

    $ virtualenv <path to venv>
    $ source <path to venv>/bin/activate
    $ cd <directory containing this file>
    $ git submodule init
    $ git submodule update
    $ cd libs/derby; python setup.py develop; cd ../..
    $ cd libs/gluten; python setup.py develop; cd ../..
    $ cd libs/pegu; python setup.py develop; cd ../..
    $ pip install ansible==1.6.2

    # under OSX 10.11, the following were required as well:
    $ cd libs
    $ git clone git@github.com:allfro/pymiproxy.git
    $ cd pymiproxy
    $ python setup.py install
    $ cd libs/proctor; python setup.py develop; cd ../../
    $ brew install openssl
    $ brew link openssl --force
    $ easy_install PyOpenSSL
    $ easy_install PyCrypto
    # end of OSX-specific install steps.

    $ python setup.py develop


Create extensions
---------

    $ psql -d scryer
    > CREATE EXTENSION unaccent;
    > CREATE EXTENSION pg_trgm;


Bootstrap
---------
    $ ../venv/bin/scryer-db wipe -c development.ini
    $ alembic -c development.ini upgrade head


Running
-------

    $ ../venv/bin/pserve --reload development.ini

Deployment
----------

Please note that you can find the vault password in the main ajah keepass file

The first time you'll have to run that command which will setup your deployment with
the server (ssh key, nginx, creating the db...):

    $ make server app_user=develop

After that you just have to prepare the event and link it:

    $ make release app_user=develop

Just change the app_user from develop to `prod` or `staging` depending of which type
of deployment you want to do.

Create User
----------
To create a new user, activate the virtualenv and run ../venv/bin/scryer-db createuser USER PASS

Instance - Create
----------
To create an instance, open "/admin/new/instance/"
If required, enter username and password ("/login")
Set instance name and slug.
Fill any of the following info and filters for this instance:

Instance - Edit
----------
Address: /admin/instance/[slug]

Browsing to /admin/instance/ allows an administrator to see instance list, select an instance for editing, and then edit and save the instance. It is also possible to select "View" for an instance.

CSV
----------
Address: /csv-import

Import CSV:
1) Browse /csv-import
2) Click "Browse" button and select CSV from file system.
3) Click "Submit Query"

Export CSV:
1) Browse to main "Landscape" screen.
2) Set filters for desired export.
3) Click "Export to CSV" button.
4) Choose location and save file.

CSV Format
----------
Here is the CSV template: https://docs.google.com/spreadsheets/d/1RbF0Mt4M7BkflQD9avB3pD6s4cFDqC_cV1Nc5kVpaHs/edit?usp=sharing

Here are the fields that should be found in the CSV:
    
    source  -  source of the funder (i.e. "ec", "hedc", "trillium", "justice", "mconnell", "swc")
    name  -  name of the organization, plain text, no line breaks, title case (i.e. "Greenpeace", "Apathy Is Boring")
    city  -  name of city, title case (i.e. "St-John", "Quebec City")
    country  -  two letter country code (i.e. "CA")
    province  -  two letter province code (i.e. "QC")
    date  -  start date format yyyy/mm/dd/ 00:00:00 (time optional)
    end_date  -  end date format yyyy/mm/dd/ 00:00:00 (time optional)
    amount  -  numeric no symbols ("12000" and not "$12,000.01")
    type  -  plain text, no line breaks (i.e. "Grant", "Contribution", "Sustainable Food Systems")
    purpose  -  description, plain text, no line breaks
    links  -  URL to "source" listing of grants & contributions
    comments  -  additional comments, plain text, no line breaks (i.e. "Multi-year contribution; 05/06 and 06/07.")